package first;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.*;


public class JSONParser {

	public static InputStream is = null;	
	public static JSONObject jObj = null;
	public static JSONArray jArray = null;
	public static String json = "";
	public static String itemjson = "";
	public static JSONObject itemjObj = null;
	// constructor
	public JSONParser() {
	}          

	public JSONObject getJSONFromUrl(String url) {

		// Making HTTP request
		try {
			// defaultHttpClient
			
			DefaultHttpClient httpClient = new DefaultHttpClient();
			
			String abc = URLEncoder.encode(url,"UTF-8");
			
			
			HttpPost httpPost = new HttpPost(url) ;
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			is = httpEntity.getContent();			

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		try {
		
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
		
			StringBuilder sb = new StringBuilder();
			
			String line = null;
			
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			
			is.close();
			
			json = null;
			json = sb.toString();
		} 
		catch (Exception e)	{}

		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e){}
		
		return jObj;

	}
	public JSONArray getJSONArrayFromUrl(String url) {

		// Making HTTP request
		try {
			// defaultHttpClient
			
			DefaultHttpClient httpClient = new DefaultHttpClient();		
			
			HttpGet httpPost = new HttpGet(url);
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			is = httpEntity.getContent();			

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {				
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);		
			StringBuilder sb = new StringBuilder();			
			String line = null;			
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}			
			is.close();
			json = null;
			json = sb.toString();
			
		} catch (Exception e) {
//			Logger.getGlobal().info(e.toString());
		}

		try {
			if(json == null || json.equals(""))
				jArray = null;
			else 
				jArray = new JSONArray(json);
			
		} catch (JSONException e) {
//			Logger.getGlobal().info(e.toString());
		}
		return jArray;
	}
	public JSONObject getJSONObjectAndArrayFromUrl(String url) {

		// Making HTTP request
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			
			String abc = URLEncoder.encode(url,"UTF-8");
			
			HttpPost httpPost = new HttpPost(url) ;
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			is = httpEntity.getContent();			

		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
		} catch (ClientProtocolException e) {
//			e.printStackTrace();
		} catch (IOException e) {
//			e.printStackTrace();
		}
		
		try {
		
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
		
			StringBuilder sb = new StringBuilder();
			
			String line = null;
			
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			
			is.close();
			
			itemjson = sb.toString();
			
		} catch (Exception e) {
//			Logger.getGlobal().info(e.toString());
		}

		try {
			itemjObj = new JSONObject(itemjson);			
		} catch (JSONException e) {
//			Logger.getGlobal().info(e.toString());
		}

		return itemjObj;

	}
}

