package first;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import adapter.ListInformation;

import first.MainDesign.ServerTimerTask;

public class BackgroundService {

	public String Url, AppId, BranchId, Password;
	public String query;
	public String isOrder, isRes, quantity;
	public String[] orderPrinterArray, resPrinterArray;
	
	public JSONParser jsonParser = new JSONParser();
	public JSONObject jsonObject, jOrderDetailObject, itemObject;
	public JSONArray jOrderArray1, itemArray;
	
	public Timer frequencyTimer;
	public FrequencyTimerTask frequencyTimerTask;
	public int period = 600000;
	
	public int order_arr_len = 0;
	public String[] orderIdArray;
	public String[] orderIdRealArray;
	public String _order_id;
	public boolean isOrderData = true;
	public static boolean stopFlag = false;
	public static boolean con = true;
	public String whichCheck;
	
	public Properties pro = null;
	
	String _branch_order_id, _order_counter;
	String _order_total, _order_paid, _order_pay_status, _order_createdon, _order_address, _order_for, _order_contact, _order_status;
	
//	String _res_branch_id, _res_user_email, _res_user_name, _res_user_phone, _res_address, _res_paid_via, _res_service_cost, _res_service_date;
//	String _res_service_madeon, _res_service_name, _res_service_paid, _res_service_time_from, _res_service_time_to, _res_status;
	
	String[] order_name_array;
	String[] order_currency_array;
	String[] order_cost_array;
	String[] order_qty_array;
	String[] order_note_array;
	
	public int tabIndex = 0;
	public Thread printThread;
	
	public static int aa=0, bb=0, cc=0, dd = 0;
	public static boolean threadFlag = false;
	
	public void settingOrderInfo() {
		
		String orderNum = pro.getProperty("orderNum");
		isOrder = pro.getProperty("orderCheck");
		quantity = pro.getProperty("quantity");
		int orderCount = Integer.parseInt(orderNum);
		orderPrinterArray = new String[orderCount];
		
        for(int i=0; i<orderCount; i++) {
        	if(isOrder.equalsIgnoreCase("unselected")) 
				break;
			
			String orderPrinter = pro.getProperty("orderPrinter"+i);
			orderPrinterArray[i] = orderPrinter;
        }
	}
	public void settingResInfo() {
		
		String resNum = pro.getProperty("resNum");
		isRes = pro.getProperty("resCheck");
		int resCount = Integer.parseInt(resNum);
		resPrinterArray = new String[resCount];
		
        for(int i=0; i<resCount; i++) {
        	if(isRes.equalsIgnoreCase("unselected")) 
				break;
			
			String resPrinter = pro.getProperty("resPrinter"+i);
			resPrinterArray[i] = resPrinter;
        }
	}
	
	public void autoPrint(String flag, int selTabIndex) {
		tabIndex = selTabIndex;
		whichCheck = flag;
		con = true;
		aa ++;
			
			try {			
				jOrderArray1 = ListInformation.jOrderArray;
				
				stopFlag = false;
				
				if(whichCheck.equals("check")) {				
					bothListShow(jOrderArray1);
				} else if(whichCheck.equals("autoCheck")) {
					if(stopFlag) {
						if(frequencyTimer != null) {
							frequencyTimer.cancel();
							frequencyTimer = null;
						}
						return;
					} else {
						if(jOrderArray1 == null || jOrderArray1.length() == 0) {
							if(stopFlag) {
								if(frequencyTimer != null) {
									frequencyTimer.cancel();
									frequencyTimer = null;
								}
								return;
							} else {
								if(frequencyTimer != null) {
									frequencyTimer.cancel();
									frequencyTimer = null;
								}
								frequencyTimer = new Timer();							
								frequencyTimerTask = new FrequencyTimerTask();
								frequencyTimer.schedule(frequencyTimerTask, 1009, period);
								return;
							}
								
						} else {
							if(threadFlag)
								return;
							bothListShow(jOrderArray1);
						}
					}
				}
			} catch(Exception e) {				
				if(frequencyTimer != null) {
					frequencyTimer.cancel();
					frequencyTimer = null;
				}
				frequencyTimer = new Timer();							
				frequencyTimerTask = new FrequencyTimerTask();
				frequencyTimer.schedule(frequencyTimerTask, 1009, period);
				return;
			} 
	}
	public void autoCheckStop() {
		stopFlag = true;
		if(frequencyTimer != null) {			
			frequencyTimer = null;			
		}
		if(frequencyTimerTask != null) {
			frequencyTimerTask = null;
		}
	}
	public void bothListShow(JSONArray jOrderArray) {
		
		bb ++ ;
		
		pro = ReadINIFile.readIt();
		if(pro != null) {
			settingOrderInfo();
			settingResInfo();
		} else {
			JOptionPane.showMessageDialog(null, "Please select printer in the Printers");
			MainDesign.hideCircleProgress();
			return;
		}
		
		try {
			int k = 0;		
			if((jOrderArray != null) &&  isOrder.equalsIgnoreCase("selected")) {
			//=======================================  Order  ============================
				order_arr_len = jOrderArray.length();
				orderIdArray = new String[order_arr_len];
				
				Properties prop_order = ReadINIFileModel.readOrderId();
				int maxVal = 0;
				if(!prop_order.isEmpty()) {
					maxVal = Integer.parseInt((String)prop_order.getProperty("orderBig"));
				}
				
				for(int i=0; i<order_arr_len; i++){					
					JSONObject childObj = jOrderArray.getJSONObject(i);					
					String orderid = childObj.getString("orderid");
					
					int ordVal = Integer.parseInt(orderid);
					if(maxVal >= ordVal)
						continue;
					
					String order_statusFlag = childObj.getString("status");
					if(order_statusFlag.equalsIgnoreCase("printed"))
						continue;
					
					orderIdArray[k] = orderid;
					k++;
				}
				for(int i=0; i<order_arr_len; i++){			
					String order = orderIdArray[i];
					if(order == null)
						order = "0";
					orderIdArray[i] = order;
				}
				if(orderIdArray.length > 0) {
					Arrays.sort(orderIdArray);
				}				
			}
			if(k == 0) 
				isOrderData = false;
			else 
				orderIdRealArray = new String[k];
			
			
			if(isOrderData == false) {//there is no printed data
				if(whichCheck.equals("check")) {
					MainDesign.hideCircleProgress();
					MainDesign.but_check.setEnabled(true);
					JOptionPane.showMessageDialog(null, "There is no any data for printing in your server");
					return;					
				} else if(whichCheck.equals("autoCheck")) {
					if(stopFlag) {
						if(frequencyTimer != null) {
							frequencyTimer.cancel();
							frequencyTimer = null;
						}
						return;
					} else { 
						if(frequencyTimer != null) {
							frequencyTimer.cancel();
							frequencyTimer = null;
						}
						frequencyTimer = new Timer();
						frequencyTimerTask = new FrequencyTimerTask();
						frequencyTimer.schedule(frequencyTimerTask, 1009, period);
						return;
					}
				}
			}	
			
			printThread = new Thread(new Runnable() {
				public void run() {       
					threadFlag = true;
					int j = 0;
					if(isOrderData) {
						for(int i=0; i<order_arr_len; i++){
		        			 String order_id = orderIdArray[i];
		        			 if(order_id.equals("0")) {
		        				 continue;
		        			 }
		        			 orderIdRealArray[j] = order_id;
		        			 j++;
		        		}
					}
					
					int len_order = 0;
					if(orderIdRealArray != null)
						len_order = orderIdRealArray.length;
					
					//============================  order Thread  ============================================================
					if(isOrderData) {
    						try {    		
								if(con) {
									con = false;
									
									if(isOrderData) {
										for(int i = 0; i < len_order; i++) {										
											if(stopFlag)
												break;										
											String orderId = orderIdRealArray[i];
											if(PrinterTypeArray.printedOrder.contains(orderId)) {												
												int _row = MainDesign.getRowFromId(orderId, MainDesign.selectedTab);
												if(MainDesign.selectedTab == 0) {
													MainDesign.table_all.setValueAt("", _row, 10);
												} else if(MainDesign.selectedTab == 1) {
													MainDesign.table_order.setValueAt("", _row, 10);
												}
												con = true;
												continue;
											} else {
												OrderPrint op= new OrderPrint(orderId, tabIndex, 1);
												op.pp();
											}
										}	
									}
								}
								Thread.sleep(200);
    						} catch (Exception e) {
    							e.printStackTrace();
    						}
    					}
					threadFlag = false;
					if(whichCheck.equals("check")) {
						MainDesign.hideCircleProgress();
						MainDesign.but_check.setEnabled(true);
						return;
					} else if(whichCheck.equals("autoCheck")) {
						if(stopFlag) {
							if(frequencyTimer != null) {
								frequencyTimer.cancel();
								frequencyTimer = null;
							}
							return;
						} else {
							if(frequencyTimer != null) {
								frequencyTimer.cancel();
								frequencyTimer = null;
							}
							frequencyTimer = new Timer();						
							frequencyTimerTask = new FrequencyTimerTask();
							frequencyTimer.schedule(frequencyTimerTask, 1009, period);
							return;
						}
					}
				}
			});
			printThread.start();
		} catch (JSONException e) {
			if(frequencyTimer != null) {
				frequencyTimer.cancel();
				frequencyTimer = null;
			}
			frequencyTimer = new Timer();						
			frequencyTimerTask = new FrequencyTimerTask();
			frequencyTimer.schedule(frequencyTimerTask, 1009, period);
			return;
		}
	
	}
	

	public class FrequencyTimerTask extends TimerTask {
		@Override
		public void run(){
//			System.out.println("startTimer2");
			if(stopFlag) {
				return;
			} else {
				if(frequencyTimer != null) {
					frequencyTimer.cancel();
					frequencyTimer = null;
				}				
				isOrderData = true;
				con = true;
				
				autoPrint("autoCheck", tabIndex);
				
			}
		}			
   }
	
}
	
   
