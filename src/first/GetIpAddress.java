package first;

import ipAdapter.Tokenizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GetIpAddress {

	public static String _gateway;
	public static String _ip;

	public static String getOwnIp() {
		try {
            Process pro = Runtime.getRuntime().exec("cmd.exe /c route print");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(pro.getInputStream())); 

            String line;
            while((line = bufferedReader.readLine())!=null)
            {
                line = line.trim();
                String [] tokens = Tokenizer.parse(line, ' ', true , true);// line.split(" ");
                if(tokens.length == 5 && tokens[0].equals("0.0.0.0")) {
                    _gateway = tokens[2];
                    _ip = tokens[3];
                    return _ip;
                }
            }
//            pro.waitFor();
			     
        }
        catch(IOException e) {
            System.err.println(e);
            e.printStackTrace();
        }
		return _ip;
	}
	public static String getOwnGetwayIp() {
		try {
            Process pro = Runtime.getRuntime().exec("cmd.exe /c route print");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(pro.getInputStream())); 

            String line;
            while((line = bufferedReader.readLine())!=null)
            {
                line = line.trim();
                String [] tokens = Tokenizer.parse(line, ' ', true , true);// line.split(" ");
                if(tokens.length == 5 && tokens[0].equals("0.0.0.0")) {
                    _gateway = tokens[2];
                    _ip = tokens[3];
                    return _gateway;
                }
            }
//            pro.waitFor();
			     
        }
        catch(IOException e) {
            System.err.println(e);
            e.printStackTrace();
        }
		return _gateway;
	}
}
