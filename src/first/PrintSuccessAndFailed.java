package first;

import java.net.URLEncoder;

import org.json.JSONObject;

public class PrintSuccessAndFailed {
	
	public String _order_id;
//	public String _res_id;
	public String Url, AppId, BranchId, Password;
	public JSONObject jsonObject;
	public JSONParser jsonParser;
	
	public PrintSuccessAndFailed(String _id, int flag) {
		if(flag == 1)
			_order_id = _id;
		jsonParser = new JSONParser();
	}
	public boolean orderPrintSuccess() {
		boolean status = false;
		try {
			String info = LoginSwingWorker.userInfoRead();
			if(!info.equals("")) {
				int a = info.lastIndexOf("/");
				int b = info.indexOf("|");
				int c = info.indexOf("=");
				Url = info.substring(0, a);
				AppId = info.substring(a+1, b);
				BranchId = info.substring(b+1, c);
				Password = info.substring(c+1);
			}
			String query = URLEncoder.encode("{" +	"\"branchid\"" +":"+BranchId+ "," + "\"app\"" +":"+AppId+ "," + "\"password\""+":"+"\""+Password+"\""+"}" ,"utf-8");
			String orderSuccessUrl = "http://" + Url + "/orders/printed/" + _order_id + "?data=" + query;
			
			jsonObject = jsonParser.getJSONFromUrl(orderSuccessUrl);
			if(jsonObject != null) {
				String checksString = jsonObject.getString("status");
				if(checksString.equalsIgnoreCase("printed")) {
					status = true;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public boolean orderPrintFailed() {
		boolean status = false;
		try {
			String info = LoginSwingWorker.userInfoRead();
			if(!info.equals("")) {
				int a = info.lastIndexOf("/");
				int b = info.indexOf("|");
				int c = info.indexOf("=");
				Url = info.substring(0, a);
				AppId = info.substring(a+1, b);
				BranchId = info.substring(b+1, c);
				Password = info.substring(c+1);
			}
			String query = URLEncoder.encode("{" +	"\"branchid\"" + ":"+BranchId + "," + "\"app\"" + ":" + AppId + "," + "\"password\"" + ":" + "\"" + Password + "\"" + "}" ,"utf-8");
			String orderFailedUrl = "http://" + Url + "/orders/failed/" + _order_id + "?data=" + query;
			jsonObject = jsonParser.getJSONFromUrl(orderFailedUrl);
			if(jsonObject != null) {
				String checksString = jsonObject.getString("status");
				if(checksString.equalsIgnoreCase("failed")) {
					status = true;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
}
