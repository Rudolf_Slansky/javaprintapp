package first;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONException;
import org.json.JSONObject;

public class GetMarkImage {
	
	public static JSONObject jsonImgObject;
	public static JSONParser jsonParser = new JSONParser();
	public static String Url, AppId, BranchId, Password;
	
	public static Image getMarkImage() {
		String query = null;
		String info = null;
		Image markImg = null;
		try {
			info = LoginSwingWorker.userInfoRead();
		
			if(!info.equals("")) {
				int a = info.lastIndexOf("/");
				int b = info.indexOf("|");
				int c = info.indexOf("=");
				Url = info.substring(0, a);
				AppId = info.substring(a+1, b);
				BranchId = info.substring(b+1, c);
				Password = info.substring(c+1);
			}
		
			query = URLEncoder.encode("{" +	"\"branchid\"" +":"+BranchId+ "," + "\"app\"" +":"+AppId+ "," + "\"password\""+":"+"\""+Password+"\""+"}" ,"utf-8");
		
			String imageUrl = "http://" + Url + "/apps/view?data=" + query;
			
			jsonImgObject = jsonParser.getJSONFromUrl(imageUrl);
			if(jsonImgObject != null) {
				String imagePath = null;
				imagePath = jsonImgObject.getString("app_icon");				
				markImg = markImage(imagePath);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return markImg;
	}

	private static Image markImage(String imagePath) {
		TrustManager[] trustAllCerts = new TrustManager[]{
		    new X509TrustManager() {
		        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		            return null;
		        }
		        public void checkClientTrusted(
		            java.security.cert.X509Certificate[] certs, String authType) {
		        }
		        public void checkServerTrusted(
		            java.security.cert.X509Certificate[] certs, String authType) {
		        }
		    }
		};

		// Activate the new trust manager
		try {
		    SSLContext sc = SSLContext.getInstance("SSL");
		    sc.init(null, trustAllCerts, new java.security.SecureRandom());
		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}
		
		BufferedImage image = null;
		try {	
			URL url = new URL(imagePath);
			image = ImageIO.read(url);
			image = resizeImage(image, 120, 120, image.TYPE_INT_ARGB);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return image;		 
	}

	private static BufferedImage resizeImage(BufferedImage originalImage, int width, int height, int type) {
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		return resizedImage;
	}
}
