package first;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.plaf.basic.BasicProgressBarUI;

public class ProgressBarUITest {
	
//	JFrame circleFrame = MainDesign.frame;
//	JPanel circlePanel;
	JProgressBar jpb;

    private static class MyProgressUI extends BasicProgressBarUI {

        private static final int ARC_EXTENT = 25;

        @Override
        protected void installDefaults() {
            super.installDefaults();
            progressBar.setBorder(null);
        }

        @Override
        protected void paintIndeterminate(Graphics g, JComponent c) {
            int angle = (int) (90 - 360.0 * getAnimationIndex() / getFrameCount());
            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            
            g.setColor(progressBar.getForeground());
            int size = 50;
            g2d.fillArc((c.getWidth() - size) / 2, (c.getHeight() - size) / 2, size, size, angle - ARC_EXTENT, ARC_EXTENT);
        }

        @Override
        protected Rectangle getBox(Rectangle r) {
            if (r != null) {
                r.setBounds(progressBar.getBounds());
                return r;
            }
            return progressBar.getBounds();
        }

        @Override
        public Dimension getPreferredSize(JComponent c) {
            return new Dimension(50, 50);
        }
    }

    public void display() {
//        circlePanel = new JPanel();
//        circlePanel.setBackground(new Color(255, 255, 255));
//        circlePanel.setLayout(new BorderLayout());
        jpb = new JProgressBar();
        jpb.setUI(new MyProgressUI());
//        jpb.setForeground(new Color(50, 205, 50));
        jpb.setForeground(new Color(0, 0, 0));
        jpb.setIndeterminate(true);
//        circleFrame.getContentPane().add(circlePanel);
//        circleFrame.setLocation(300, 200);
//        circleFrame.setSize(1024, 768);
//        circleFrame.setVisible(true);
    }    
    public void nonDsiplay() {
    	jpb.setVisible(false);
    }
}