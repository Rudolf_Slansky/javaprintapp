//package first;
//
//import java.awt.*;
//import java.awt.event.*;
//
//import javax.print.Doc;
//import javax.print.DocFlavor;
//import javax.print.DocPrintJob;
//import javax.print.PrintService;
//import javax.print.SimpleDoc;
//import javax.print.attribute.Attribute;
//import javax.swing.*;
//
//
//import Util.DataConstants;
//import Util.Util;
//import adapter.ListInformation;
//import adapter.NewsItem;
//
//import java.awt.print.*;
//import java.util.ArrayList;
//import java.util.Properties;
// 
//public class ResPrint implements Printable {
//
//	public String _res_id;
//	public int _row;
//	public int _selectedTab;
//	public String[] resPrinterArray;
//	public boolean isPrinted = false;
//	public String count;
//	public int _finalFlag;
//   
//	public ResPrint(String resId, int tabIndex, int finalFlag) {
//		_res_id = resId;
//    	_selectedTab = tabIndex;
//    	_finalFlag = finalFlag;
//	}
//
//	public String resText(String resId) {
//    	
//    	ArrayList<NewsItem> resDetail = new ArrayList<NewsItem>();		
//		resDetail = ListInformation.detailResData(resId);
//		
//		String _res_branch_id = resDetail.get(0).getResBranchId();
//		String _res_user_email = resDetail.get(0).getResEmail();
//		String _res_user_name = resDetail.get(0).getResName();
//		String _res_user_phone = resDetail.get(0).getResPhone();
//		String _res_address = resDetail.get(0).getResAddress();
//		String _res_paid_via = resDetail.get(0).getResPaid();
//		String _res_service_cost = resDetail.get(0).getResCost();
//		String _res_service_date = resDetail.get(0).getResDate();
//		String _res_service_madeon = resDetail.get(0).getResMadeon();
//		String _res_service_name = resDetail.get(0).getResSName();
//		String _res_service_paid = resDetail.get(0).getResSPaid();
//		String _res_service_time_from = resDetail.get(0).getResTimeFrom();
//		String _res_service_time_to = resDetail.get(0).getResTimeTo();
//		String _res_status = resDetail.get(0).getResStatus();
//		
//		String service_time = " From "+_res_service_time_from+" to "+_res_service_time_to;
//		//address : Marco Polo Eastfields Avenue Wandsworth Riverside,London SW18 1LP, London, SW18 1LP
//		String _res_address1 = _res_address.substring(0, _res_address.indexOf(","));
//		String _res_address2 = _res_address.substring(_res_address.indexOf(",")+1);
//		String receipt = "-------------------------   RESERVATION   -------------------------;"
//							+ "                                  " + ";"
//							+ "Reservation ID : " + _res_branch_id + ";"
//							+ "System ID        : " + resId + ";"
//							+ "Status              : " + _res_status + ";"
//							+ "User Email       : " + _res_user_email + ";"
//							+ "User Name      : " + _res_user_name + ";"
//							+ "User Phone     : " + _res_user_phone + ";"
//							+ "Address  : " + _res_address1 + ";"
//							+ "                 " + _res_address2 + ";"
//							+ "Paid Via           : " + _res_paid_via + ";"
//							+ "Service Cost    : " + _res_service_cost + ";"
//							+ "Service Date    : " + _res_service_date + ";"
//							+ "Service Time    : " + _res_service_madeon + ";"
//							+ "Service Name   : " + _res_service_name + ";"
//							+ "Service Paid     : " + _res_service_paid + ";"
//							+ "Service Time    : " + service_time + ";"
//							+ "------------------------------------------------------------;"
//							+ "------------------------------------------------------------;"
//							+ "------------------------------------------------------------;";
//		return receipt;    	
//    }
//	@Override
//    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
//        if (pageIndex > 0) { /* We have only one page, and 'page' is zero-based */
//            return NO_SUCH_PAGE;
//        }
//
//        Graphics2D g2d = (Graphics2D) graphics;
//        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
//        
//		String receipt = resText(_res_id);
//        String[] bill = receipt.split(";");
//        int y = 15;
//        Font f = new Font(Font.SANS_SERIF, Font.PLAIN, 12);
//        graphics.setFont(f);
//        //draw each String in a separate line
//        for (int i = 0; i < bill.length; i++) {
//            graphics.drawString(bill[i], 50, y);
//            y = y + 15;
//        }
//
//        return PAGE_EXISTS;
//    }
//
//    public void pp() {
//    	
//    	Properties pro = ReadINIFile.readIt();
//    	
//		String resNum = pro.getProperty("resNum");
//		String resCheck = pro.getProperty("resCheck");
//		
//		int resCount = Integer.parseInt(resNum);
//		
//		resPrinterArray = new String[resCount];	
//		PrinterService ps = new PrinterService();
//		
//		for(int i=0; i<resCount; i++) {
//			if(resCheck.equalsIgnoreCase("unselected"))
//				break;
//			
//			String resPrinter = pro.getProperty("resPrinter"+i);
//			resPrinterArray[i] = resPrinter;
//			final PrintService pss = ps.getCheckPrintService(resPrinterArray[i].toString());
//			
//			if(pss == null)
//				continue;
//			
//			final PrinterJob job = PrinterJob.getPrinterJob();
//	        try {
//				job.setPrintService(pss);
//				job.setPrintable(this);
//			} catch (PrinterException e) {
//				e.printStackTrace();
//			}
//	        Thread t = new Thread(new Runnable() {
//				@Override
//				public void run() {
//					try {
//						//==========
////						if(MainDesign.manualPrintFlag) {// only when manual printing, open cash drawer
////							try {		
////								DocPrintJob doc_job = pss.createPrintJob();
////								
////								byte[] cmd = new byte[]{0x1B, 0x70, 0x00, 60, 120};
////								
////								DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
////								
////								Doc doc = new SimpleDoc(cmd, flavor, null);
////								
////								doc_job.print(doc, null);
////								
////							} catch (Exception e) {					
////								e.printStackTrace();	
////							}
////						}
//						//=========
//						job.print();		
//						isPrinted = true;						
//					} catch (PrinterException e) {
//						e.printStackTrace();
//						isPrinted = false;
//					} 
//					
//					while (true) {
//						for(Attribute a : pss.getAttributes().toArray()) {
//			            	if(a.getName().equalsIgnoreCase("queued-job-count")) {
//			            		count = a.toString();
//			            		break;
//			            	}
//			            }
//						if(Integer.parseInt(count) == 0) {
//							break;
//						}
//						try {
//							Thread.sleep(300);
//						} catch (InterruptedException e) {
//							e.printStackTrace();
//						}
//					}
//					
//					PrintSuccessAndFailed resP = new PrintSuccessAndFailed(_res_id, 2);
//					
//					
//					_row = MainDesign.getRowFromId(_res_id, _selectedTab);
//					if(_row == -1) {
//						BackgroundService.con = true;
//						return;
//					}
//					
//					if(Integer.parseInt(count) == 0 && isPrinted) {								
//						if(resP.resPrintSuccess()) {
//							
//							PrinterTypeArray.printedRes.add(_res_id);
//							
//							if(_selectedTab == 0) {
//								MainDesign.table_all.setValueAt("", _row, 10);
//							} else if(_selectedTab == 1) {
//								MainDesign.table_order.setValueAt("", _row, 10);
//							} else if(_selectedTab == 2) {
//								MainDesign.table_res.setValueAt("", _row, 7);
//							}
//						}	
//					} else {
//						if(resP.resPrintFailed()) {
//							
//							String sign = "<html><span style=\"color:red; font-size:15px;\">" + "!" + "</span></html>";
//							
//							if(_selectedTab == 0) {
//								MainDesign.table_all.setValueAt(sign, _row, 10);
//							} else if(_selectedTab == 1) {
//								MainDesign.table_order.setValueAt(sign, _row, 10);
//							} else if(_selectedTab == 2) {
//								MainDesign.table_res.setValueAt(sign, _row, 7);
//							}
//						}
//					}
//					if(_finalFlag == 1) {
//						if(Integer.parseInt(count) == 0) {
//							BackgroundService.con = true;
//						}
//					}
//						
//				}
//	        });
//	        t.start();
//	        try {
//				t.join();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//        
//    }
//}
