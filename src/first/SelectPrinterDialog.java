package first;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Properties;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class SelectPrinterDialog extends JPanel {
	
	public static JDialog printerDialog;
	public static RoundedPanel panel_order;
	public static RoundedPanel panel_wel;
	public static JLabel lblNewLabel;
	public static JList list_order;
	public static JCheckBox chk_order;//, chk_res;
//	public static JComboBox cashNumComboBox, cashPrinterType;
	public static JButton but_save, testingButton;
	public static String _ip;
	public static String hostName;
	public static String[] hostNameArray;
	public static JScrollPane scrollPane_o, testingScrollPane;
//	public static JLabel lblReservationSelectPrinter;
	
	public static String[] dest_order;
	public static int[] selectedIndex_order;
	public static int len1 = 0, len2 = 0;
	public static String o_chk_val = "unselected";
	
	public static String[] o_data;
	public static int o_len = 0;
	public static int[] selIndex;
		
	public static boolean orderClickFlag = false;
	
//	public static String selectedCashType, selectedCashNum, selectedCashPrinterName, selectedNotListedPrinterCode;	
	
//	public static JTable cashTable;
//	public static DefaultTableModel cashTableModel;
	public static DefaultTableCellRenderer cr = new DefaultTableCellRenderer();
	public static boolean DEBUG = false;
	public static JLabel printer_select_label;
//	public static JLabel cash_drawer_select;
	public static JLabel confirm_check;
	private static JLabel lblEnterNameOf;
//	private static JTextField cashPrinterNameTextField;
	private static JLabel lblPleaseEnterCode;
//	private static JTextField notListedPrinterCodeTextField;
	
	public static JList testingList;
	public static String selectedPrinter;
	
//	public static String cashNum, cashPrinterName, notListedCode;
		
	public static String[] createHostNameArray() {
		DocFlavor myFormat = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
		PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
		
		PrintService[] printService = PrintServiceLookup.lookupPrintServices(myFormat, aset);
		
		int len = printService.length;
		hostNameArray = new String[len];
		int k = 0;
		for(PrintService printer : printService) {
			hostNameArray[k] = printer.getName();
			k++;
		}
		return hostNameArray;
	}
	public static String[] getPrinterConnectedCashDrawer() {
		
		return null;
	}
	public static int isExistItem(JList list, String val) {
		int ix = list.getModel().getSize();
		int k = -1;
		for(int i=0; i<ix; i++) {
			String obj = (String) list.getModel().getElementAt(i);
			if(obj.equals(val)){
				k = i;
				break;
			}
		}
		return k;
	}
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void createAndShowDialogGUI() {
		
		cr.setHorizontalAlignment(JLabel.CENTER);
		
		try {
			Properties pro = new Properties();
			File f = new File(ReadINIFile.currentPathString + "\\temp.properties");
			if(f.isFile()) {				
				pro.load(new FileInputStream(ReadINIFile.currentPathString + "\\temp.properties"));
				o_chk_val = pro.getProperty("orderCheck");
				
				if(o_chk_val == null)
					o_chk_val = "unselected";
				
				String orderNum = pro.getProperty("orderNum");
				if(orderNum != null)
					o_len = Integer.parseInt(orderNum);
				
				o_data = new String[o_len];
				for(int i=0; i<o_len; i++) {
					o_data[i] = pro.getProperty("orderPrinter"+i);
				}				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		printerDialog = new JDialog();
		printerDialog.setModal(true);
		printerDialog.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 14));
		printerDialog.getContentPane().setBackground(new Color(17, 215, 184));
		printerDialog.getContentPane().setLayout(null);				 
		printerDialog.setSize(900, 700);
		printerDialog.setResizable(false);
		
		panel_wel = new RoundedPanel();
		panel_wel.setBackground(new Color(61, 111, 106));
		printerDialog.getContentPane().add(panel_wel);
		panel_wel.setLayout(null);
		
		panel_order = new RoundedPanel();
		panel_order.setBackground(new Color(61, 111, 106));
		panel_order.setBounds(29, 93, 832, 505);
		printerDialog.getContentPane().add(panel_order);
		panel_order.setLayout(null);
		
		
		lblNewLabel = new JLabel("Select Order Printer");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(51, 167, 154, 26);
		panel_order.add(lblNewLabel);
		
		JLabel lblConfirmOrdersAre = new JLabel("Confirm Orders are Required");
		lblConfirmOrdersAre.setForeground(Color.WHITE);
		lblConfirmOrdersAre.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblConfirmOrdersAre.setBounds(119, 45, 225, 26);
		panel_order.add(lblConfirmOrdersAre);
		
		chk_order = new JCheckBox();
		if(o_chk_val != null) {
			if(o_chk_val.equals("selected"))
				chk_order.setSelected(true);
		}
		chk_order.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				JCheckBox o_chk = (JCheckBox)event.getSource();
				if(o_chk.isSelected()){					
					o_chk_val = "selected";
				} else {
					o_chk_val = "unselected";
				}
			}
		});
		chk_order.setBounds(51, 48, 21, 23);
		panel_order.add(chk_order);
		
//		JLabel lblConfirmReservationsAre = new JLabel("Confirm Reservations are Required");
//		lblConfirmReservationsAre.setBounds(119, 86, 255, 26);
//		panel_order.add(lblConfirmReservationsAre);
//		lblConfirmReservationsAre.setForeground(Color.WHITE);
//		lblConfirmReservationsAre.setFont(new Font("Tahoma", Font.BOLD, 14));
//		
//		chk_res = new JCheckBox();
//		if(r_chk_val != null) {
//			if(r_chk_val.equals("selected"))
//				chk_res.setSelected(true);
//		}
//		chk_res.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent event) {
//				JCheckBox r_chk = (JCheckBox)event.getSource();
//				if(r_chk.isSelected()) {
//					r_chk_val = "selected";
//				} else {
//					r_chk_val = "unselected";
//				}
//			}
//		});
//		chk_res.setBounds(51, 89, 21, 23);
//		panel_order.add(chk_res);
		
	// Order List
		scrollPane_o = new JScrollPane();
		scrollPane_o.setBounds(51, 199, 330, 110);
		panel_order.add(scrollPane_o);	
				list_order = new JList(createHostNameArray());
		scrollPane_o.setViewportView(list_order);
		list_order.setSelectionModel(new DefaultListSelectionModel() {
			@Override
			public void setSelectionInterval(int index0, int index1) {
				if(super.isSelectedIndex(index0))
					super.removeSelectionInterval(index0, index1);
				else
					super.addSelectionInterval(index0, index1);
			}
		});
		
		list_order.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				orderClickFlag = true;
				Object obj[] = list_order.getSelectedValues();
				len1 = obj.length;
				dest_order = new String[len1];
				selectedIndex_order = list_order.getSelectedIndices();	
				for(int i=0; i<len1; i++){
					dest_order[i] = (String)obj[i];
					selectedIndex_order[i] = i;					
				}
			}
		});
		list_order.setFont(new Font("Tahoma", Font.BOLD, 12));
		dest_order = new String[o_len];
		
		for(int i=0; i<o_len; i++) {
			String selVal = o_data[i];
			int index = isExistItem(list_order, selVal);
			list_order.getSelectionModel().setSelectionInterval(index, index);
			dest_order[i] = selVal;
		}		
		
		
//		lblReservationSelectPrinter = new JLabel("Select Reservation Printer");
//		lblReservationSelectPrinter.setForeground(Color.WHITE);
//		lblReservationSelectPrinter.setFont(new Font("Tahoma", Font.BOLD, 14));
//		lblReservationSelectPrinter.setBounds(51, 320, 225, 26);
//		panel_order.add(lblReservationSelectPrinter);
//		if(quantity == null)
//			spinner_quantity.setValue(new Integer(1));
//		else 
//			spinner_quantity.setValue(new Integer(quantity));
		
//		JLabel orderPrinterType = new JLabel("Select type of Printer");
//		orderPrinterType.setForeground(Color.WHITE);
//		orderPrinterType.setFont(new Font("Tahoma", Font.BOLD, 14));
//		orderPrinterType.setBounds(474, 190, 192, 26);
//		panel_order.add(orderPrinterType);
		
//		cashPrinterType = new JComboBox(PrinterTypeArray.printerTypePre);
//		cashPrinterType.setBounds(474, 215, 309, 30);
//		cashPrinterType.setSelectedItem(selectedCashType);
//		selectedCashType = (String)cashPrinterType.getSelectedItem();
//		panel_order.add(cashPrinterType);
//		cashPrinterType.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				JComboBox jcom = (JComboBox)e.getSource();
//				selectedCashType = (String)jcom.getSelectedItem();
//				if(!selectedCashType.equals("")) {
//					cashPrinterNameTextField.setEnabled(false);
//					cashPrinterNameTextField.setText("");
//					cashNumComboBox.setEnabled(false);
//					cashNumComboBox.setSelectedItem("");
//					notListedPrinterCodeTextField.setEnabled(false);
//					notListedPrinterCodeTextField.setText("");
//				} else {
//					//selectedCashType, selectedCashNum, selectedCashPrinterName, selectedNotListedPrinterCode;
//					cashPrinterNameTextField.setEnabled(true);
//					cashPrinterNameTextField.setText(selectedCashPrinterName);
//					cashNumComboBox.setEnabled(true);
//					cashNumComboBox.setSelectedItem(selectedCashNum);
//					notListedPrinterCodeTextField.setEnabled(true);
//					notListedPrinterCodeTextField.setText(selectedNotListedPrinterCode);
//				}
//			}			
//		});
		
//		cashPrinterNameTextField = new JTextField();
//		cashPrinterNameTextField.setText(selectedCashPrinterName);
//		cashPrinterNameTextField.setColumns(10);
//		cashPrinterNameTextField.setBounds(474, 277, 308, 30);		
//		if(selectedCashType != null && !selectedCashType.equals("")) {
//			cashPrinterNameTextField.setEnabled(false);
//		}
//		panel_order.add(cashPrinterNameTextField);
	//=================================================================================================================================
//		String v = "<html>Please enter the cash draer number for this <br>instance of the application</html>";
//		JLabel orderModel = new JLabel(v);
//		orderModel.setForeground(Color.WHITE);
//		orderModel.setFont(new Font("Tahoma", Font.BOLD, 14));
//		orderModel.setBounds(474, 398, 318, 41);
//		panel_order.add(orderModel);
//		
//		printer_select_label = new JLabel("");
//		printer_select_label.setBounds(29, 145, 384, 337);
//		TitledBorder titled = new TitledBorder("Select Printers");
//		titled.setTitleColor(Color.WHITE);
//		titled.setTitleFont(new Font("Tahoma", Font.BOLD, 14));
//		printer_select_label.setBorder(titled);
//		panel_order.add(printer_select_label);
//		
//		cash_drawer_select = new JLabel("");
//		cash_drawer_select.setBounds(442, 179, 356, 303);
//		TitledBorder titled_cash = new TitledBorder("Cash Drawer Opening Set Up");
//		titled_cash.setTitleColor(Color.WHITE);
//		titled_cash.setTitleFont(new Font("Tahoma", Font.BOLD, 14));
//		cash_drawer_select.setBorder(titled_cash);
//		panel_order.add(cash_drawer_select);
//		
//		confirm_check = new JLabel("");
//		confirm_check.setBounds(29, 19, 384, 110);
//		TitledBorder titled_confirm = new TitledBorder("Confirm Set Up");
//		titled_confirm.setTitleColor(Color.WHITE);
//		titled_confirm.setTitleFont(new Font("Tahoma", Font.BOLD, 14));
//		confirm_check.setBorder(titled_confirm);
//		panel_order.add(confirm_check);
//		
//		JLabel spiner_label = new JLabel("");
//		spiner_label.setBounds(442, 19, 356, 152);
//		TitledBorder spinerTitled = new TitledBorder("Select printer for opening cash drawer");
//		spinerTitled.setTitleColor(Color.WHITE);
//		spinerTitled.setTitleFont(new Font("Tahoma", Font.BOLD, 14));
//		spiner_label.setBorder(spinerTitled);
//		panel_order.add(spiner_label);
//		
//		lblEnterNameOf = new JLabel("Enter name of Printer");
//		lblEnterNameOf.setForeground(Color.WHITE);
//		lblEnterNameOf.setFont(new Font("Tahoma", Font.BOLD, 14));
//		lblEnterNameOf.setBounds(474, 254, 289, 26);
//		panel_order.add(lblEnterNameOf);
//	
//		//cash Number ComboBox		
//		cashNumComboBox = new JComboBox(PrinterTypeArray.cashNumber);
//		cashNumComboBox.setMaximumRowCount(15);
//		cashNumComboBox.setSelectedItem(selectedCashNum);
//		cashNumComboBox.setBounds(474, 440, 136, 30);
//		panel_order.add(cashNumComboBox);
//		
//		if(selectedCashType != null && !selectedCashType.equals("")) {
//			cashNumComboBox.setEnabled(false);
//		}
//		
//		lblPleaseEnterCode = new JLabel("<html>Please enter Code for not listed printer <br>to open cash drawer</html>");
//		lblPleaseEnterCode.setForeground(Color.WHITE);
//		lblPleaseEnterCode.setFont(new Font("Tahoma", Font.BOLD, 14));
//		lblPleaseEnterCode.setBounds(474, 322, 308, 36);
//		panel_order.add(lblPleaseEnterCode);
//		
//		//notListedPrinter code InputBox
//		notListedPrinterCodeTextField = new JTextField();
//		notListedPrinterCodeTextField.setText(selectedNotListedPrinterCode);
//		notListedPrinterCodeTextField.setColumns(10);
//		notListedPrinterCodeTextField.setBounds(474, 362, 308, 30);
//		if(selectedCashType != null && !selectedCashType.equals("")) {
//			notListedPrinterCodeTextField.setEnabled(false);
//		}
//		panel_order.add(notListedPrinterCodeTextField);
//		
//		JLabel testingPrinterLabel = new JLabel("Select the printer the cash drawer is linked to");
//		testingPrinterLabel.setForeground(Color.WHITE);
//		testingPrinterLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
//		testingPrinterLabel.setBounds(474, 38, 323, 26);
//		panel_order.add(testingPrinterLabel);
//		
//		testingScrollPane = new JScrollPane();
//		testingScrollPane.setBounds(474, 64, 308, 65);
//		panel_order.add(testingScrollPane);
//		
//		//testing printer list
//		testingList = new JList(createHostNameArray());
//		testingScrollPane.setViewportView(testingList);
//		
//		//selection mode(multi selection allow)
////		testingList.setSelectionModel(new DefaultListSelectionModel() {
////			@Override
////			public void setSelectionInterval(int index0, int index1) {
////				if(super.isSelectedIndex(index0))
////					super.removeSelectionInterval(index0, index1);
////				else
////					super.addSelectionInterval(index0, index1);
////			}
////		});
//		testingList.addListSelectionListener(new ListSelectionListener() {
//			@Override
//			public void valueChanged(ListSelectionEvent event) {
//				if(!event.getValueIsAdjusting()) {
//					selectedPrinter = testingList.getSelectedValue().toString();		
//					System.out.println("selected");
//				}
//			}
//		});
//		
//		//"Open Drawer" Button
//		testingButton = new JButton("Open Drawer");
//		testingButton.setBounds(579, 138, 108, 23);
//		panel_order.add(testingButton);
//		
//		testingButton.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent event) {
//				System.out.println(selectedPrinter);
//				if(selectedPrinter == null) {
//					MainDesign.showMessage(printerDialog, "Please select printer for opening cash drawer");
//					return;
//				}
//				MainDesign.getCashDrawerCode();
//				OpenCashDrawer.OpenDrawer(selectedPrinter);
////				if(OpenCashDrawer.getStatus()) {
////					MainDesign.showMessage(printerDialog, "Sent data to cash drawer");
////				}
//			}
//		});
//		
//		cashNumComboBox.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent event) {
//				JComboBox jcom = (JComboBox)event.getSource();
//				selectedCashNum = (String)jcom.getSelectedItem();
//			}
//		});
		
	// Save Button
		but_save = new JButton("Save");
		but_save.setFont(new Font("Tahoma", Font.BOLD, 16));
		but_save.setForeground(new Color(255, 255, 255));
		but_save.setBackground(new Color(47, 79, 79));
		but_save.setBounds(395, 609, 127, 39);
		but_save.setCursor(new Cursor(Cursor.HAND_CURSOR));
		printerDialog.getContentPane().add(but_save);
		
		but_save.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event) {
				int orderLen = 0;
				if(orderClickFlag) {
					orderLen = len1;
				} else {
					orderLen = o_len;
				}
				
				
				if(orderLen > 0) {
					ReadINIFile.writeIt(dest_order, o_chk_val);
				} else if(orderLen == 0){
					JOptionPane.showMessageDialog(null, "Please select printer for Order and Reservation");
					return;
				}
				printerDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				printerDialog.setVisible(false);
			}
		});		
		
		panel_wel = new RoundedPanel();
		panel_wel.setBounds(29, 36, 832, 59);
		printerDialog.getContentPane().add(panel_wel);
		panel_wel.setLayout(null);
		panel_wel.setBackground(new Color(61, 111, 106));
		
		JLabel lblSelectPrintersTo = new JLabel("Select Printers to print Orders to :");
		lblSelectPrintersTo.setForeground(Color.WHITE);
		lblSelectPrintersTo.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSelectPrintersTo.setBounds(36, 11, 602, 37);
		panel_wel.add(lblSelectPrintersTo);
				
		printerDialog.setLocationRelativeTo(null);
		printerDialog.setVisible(true);
		
	}
}
