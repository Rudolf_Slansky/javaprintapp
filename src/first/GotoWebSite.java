package first;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

//import com.teamdev.jxbrowser.chromium.Browser;
//import com.teamdev.jxbrowser.chromium.swing.BrowserView;

public class GotoWebSite {

	/**
	 * @wbp.parser.entryPoint
	 */
	public static void createAndShowDialogGUI() {
		Properties pro = ReadINIFileModel.readWebUrl();
		String realUrl = pro.getProperty("webUrl");
		if(realUrl != null) {
			String val = realUrl.substring(realUrl.lastIndexOf("/")+1);
			
			if(!val.equals("")) {
				if(realUrl.startsWith("http://") || realUrl.startsWith("https://")) {
					try {
						URL webUrl = new URL(realUrl);
						openWebPage(webUrl.toURI());
//						openWebsite(realUrl);
					} catch (MalformedURLException e1) {
						e1.printStackTrace();
					} catch (URISyntaxException e1) {
						e1.printStackTrace();
					}
				}
			} else {
				JOptionPane.showMessageDialog(null, "Please enter valid URL in Set up");
			}
		} else {
			JOptionPane.showMessageDialog(null, "Please enter valid URL in Set up");
		}
	}

//	private static void openWebsite(String realUrl) {
//		Browser browser = new Browser();
//        BrowserView browserView = new BrowserView(browser);
//
//        JDialog web_dialog = new JDialog();
//        web_dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//        web_dialog.setModal(true);
//        web_dialog.setResizable(true);
////        web_dialog.setBounds(170, 225, MainDesign.mainW-MainDesign.leftW-21, MainDesign.mainH-MainDesign.topH-114);
//        web_dialog.add(browserView, BorderLayout.CENTER);
//        web_dialog.setSize(MainDesign.mainW-MainDesign.leftW, MainDesign.mainH-MainDesign.topH);
//        web_dialog.setLocationRelativeTo(null);
//        web_dialog.setVisible(true);
//
//        browser.loadURL(realUrl);
//	}

	protected static void openWebPage(URI uri) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if(desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(uri);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
