//package first;
//
//import java.awt.*;
//import java.awt.event.*;
//
//import javax.print.PrintService;
//import javax.swing.*;
//
//import adapter.ListInformation;
//import adapter.NewsItem;
//
//import java.awt.print.*;
//import java.util.ArrayList;
//import java.util.Properties;
//
//public class ResPrintCheck implements Printable {
//
//    public int[] pageBreaks;
//    public String[] resPrinterArray;
//    public String[] resPrintTextArray;
//    public ArrayList<NewsItem> resListArray;
//    public String[] printLine;
//    
//    public ResPrintCheck(ArrayList<NewsItem> resResultArray) {
//    	resListArray = resResultArray;
//	}
//
//    public String[] resText(ArrayList<NewsItem> resResultArray) {    	
//    	int len = resResultArray.size();
//    	String[] resIdArray = new String[len];
//    	for(int i=0; i<len; i++) {
//    		String res_id = resResultArray.get(i).getReservationId();
//    		boolean isNotPrinted = resResultArray.get(i).getEMark();
//    		if(isNotPrinted)
//    			resIdArray[i] = res_id;
//    	}
//    	
//    	int resCount = resIdArray.length;
//    	resPrintTextArray = new String[resCount];
//    	
//    	for(int i=0; i<resCount; i++) {
//    		ArrayList<NewsItem> resDetail = new ArrayList<NewsItem>();		
//    		resDetail = ListInformation.detailResData(resIdArray[i]);
//    		
//    		String _res_branch_id = resDetail.get(0).getResBranchId();
//    		String _res_user_email = resDetail.get(0).getResEmail();
//    		String _res_user_name = resDetail.get(0).getResName();
//    		String _res_user_phone = resDetail.get(0).getResPhone();
//    		String _res_address = resDetail.get(0).getResAddress();
//    		String _res_paid_via = resDetail.get(0).getResPaid();
//    		String _res_service_cost = resDetail.get(0).getResCost();
//    		String _res_service_date = resDetail.get(0).getResDate();
//    		String _res_service_madeon = resDetail.get(0).getResMadeon();
//    		String _res_service_name = resDetail.get(0).getResSName();
//    		String _res_service_paid = resDetail.get(0).getResSPaid();
//    		String _res_service_time_from = resDetail.get(0).getResTimeFrom();
//    		String _res_service_time_to = resDetail.get(0).getResTimeTo();
//    		String _res_status = resDetail.get(0).getResStatus();
//    		
//    		String service_time = " From "+_res_service_time_from+" to "+_res_service_time_to;
//    		
//    		String receipt = "----------- RESERVATION -----------;"
//    							+ "Reservation ID : " + _res_branch_id + ";"
//    							+ "System ID      : " + resIdArray[i] + ";"
//    							+ "Status         : " + _res_status + ";"
//    							+ "User Email     : " + _res_user_email + ";"
//    							+ "User Name      : " + _res_user_name + ";"
//    							+ "User Phone     : " + _res_user_phone + ";"
//    							+ "Address        : " + _res_address + ";"
//    							+ "Paid Via       : " + _res_paid_via + ";"
//    							+ "Service Cost   : " + _res_service_cost + ";"
//    							+ "Service Date   : " + _res_service_date + ";"
//    							+ "Service Time   : " + _res_service_madeon + ";"
//    							+ "Service Name   : " + _res_service_name + ";"
//    							+ "Service Paid   : " + _res_service_paid + ";"
//    							+ "Service Time   : " + service_time + ";"
//    							+ "----------------------------------------------;"
//    							+ "----------------------------------------------;"
//    							+ "----------------------------------------------;";
//    		resPrintTextArray[i] = receipt;
//    	}
//    	
//		return resPrintTextArray;    	
//    }
//    public int print(Graphics g, PageFormat pf, int pageIndex) throws PrinterException {
//
//        Font font = new Font("Serif", Font.PLAIN, 10);
//        FontMetrics metrics = g.getFontMetrics(font);
//        int lineHeight = metrics.getHeight();
//        int lineCount = 0;
//        String[] printData = resText(resListArray);
//        int len = printData.length;
//        if (pageBreaks == null) {
//        	for(int i=0; i<printData.length; i++) {
//            	String receipt = printData[i];
//            	String[] bill = receipt.split(";");
//            	lineCount = lineCount + bill.length;
//            }
//            
//        	int index = 0;
//            printLine = new String[lineCount];
//            for(int i = 0; i < len; i++) {
//            	String receipt = printData[i];
//            	String[] bill = receipt.split(";");
//            	for(int j = 0; j < bill.length; j++) {
//            		String d = bill[j];
//            		printLine[index] = d;
//            		index++;
//            	}
//            }
////            System.out.println(""+lineCount);
//            
//            int linesPerPage = (int)(pf.getImageableHeight()/lineHeight);
//            int numBreaks = (printLine.length-1)/linesPerPage;
//            pageBreaks = new int[numBreaks];
//            for (int b=0; b<numBreaks; b++) {
//                pageBreaks[b] = (b+1)*linesPerPage; 
//            }
//        }
//
//        if (pageIndex > pageBreaks.length) {
//            return NO_SUCH_PAGE;
//        }
//
//        Graphics2D g2d = (Graphics2D)g;
//        g2d.translate(pf.getImageableX(), pf.getImageableY());
//        int l = pageBreaks.length;
//        int y = 0; 
//        int start = (pageIndex == 0) ? 0 : pageBreaks[pageIndex-1];
//        int end   = (pageIndex == pageBreaks.length) ? lineCount : pageBreaks[pageIndex];
//        for (int k = start; k < end; k++) {
////        for(int j=0; j<printData.length; j++) {
////        	String[] bill = printData[j].split(";");
////        	for(int k=0; k<bill.length; k++) {
//        		y += lineHeight;
//        		g.drawString(printLine[k], 0, y);
////        		System.out.println(printLine[k]);
////        	}
////        }
//        }
////        System.out.println(lineCount);
//        return PAGE_EXISTS;
//    }
//    public void pp() {
//    	Properties pro = ReadINIFile.readIt();
//		String resNum = pro.getProperty("resNum");
//		String resCheck = pro.getProperty("resCheck");
//		final String isAlert = pro.getProperty("alertCheck");
//		int resCount = Integer.parseInt(resNum);
//		
//		resPrinterArray = new String[resCount];
//		
//        PrinterService ps = new PrinterService();
//        
//        
//        for(int i=0; i<resCount; i++) {
//        	if(resCheck.equalsIgnoreCase("unselected")) 
//				break;
//			
//			String resPrinter = pro.getProperty("resPrinter"+i);
//			resPrinterArray[i] = resPrinter;
//			final PrintService pss = ps.getCheckPrintService(resPrinterArray[i].toString());
//			
//			if(pss == null)
//				continue;
//			final PrinterJob job = PrinterJob.getPrinterJob();
//			
//			try {
//				job.setPrintService(pss);
//			} catch (PrinterException e) {
//				e.printStackTrace();
//	        }
//			
//			Book  book = new Book();
//	     	book.append(this, job.defaultPage());
//	     	job.setPageable(book);
//	        job.setPrintable(this);
//	        
//			new Thread(new Runnable() {
//				@Override
//				public void run() {
//					try {
////						if(isAlert.equalsIgnoreCase("selected")) {
////							AudioPlayer a = new AudioPlayer();
////							a.playBack();
////						}
//						job.print();
//					} catch (PrinterException e) {
//						e.printStackTrace();
//					}
//				}
//	        }).start();
//			
//        }
//    }    
//}
