package first;

import java.io.FileWriter;

public class UserInfo {
	
	public static String url;
	public static String appId;
	public static String branchId;
	public static String password;
	
	//url
	public void setUrl(String murl) {
		url = murl;
	}
	public static String getUrl() {
		return url;
	}
	//appId
	public void setAppId(String mAppId) {
		appId = mAppId;
	}
	public static String getAppId() {
		return appId;
	}
	//branchId
	public void setBranchId(String mBranchId) {
		branchId = mBranchId;
	}
	public static String getBranchId() {
		return branchId;
	}
	//password
	public void setPassword(String mPass) {
		password = mPass;
	}
	public static String getPassword() {
		return password;
	}
	
}