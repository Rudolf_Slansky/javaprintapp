package first;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

public class ButtonRenderer extends DefaultTableCellRenderer {
	  
	  public ButtonRenderer() {
	    setOpaque(true);
	  }
	  @Override
	  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	    if (isSelected) {
	      setForeground(table.getSelectionForeground());
	      setBackground(table.getSelectionBackground());
	      oneItemPrint(row, column);
	    } else{
	      setForeground(table.getForeground());
	      setBackground(UIManager.getColor("Button.background"));
	    }
	    setText( (value ==null) ? "" : value.toString() );
	    return this;
	  }

	private void oneItemPrint(int row, int column) {
		String order_id = MainDesign.resultArray.get(row).getOrderId();
	}
}
