package first;

import java.util.Enumeration;

import javax.comm.CommPort;
import javax.comm.CommPortIdentifier;
import javax.comm.PortInUseException;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterName;


// Check each port to see if it is open. /
public class OpenPort {
	
	String commandSet;

	public static void findPort() {
	    java.util.Enumeration<CommPortIdentifier> port_list = CommPortIdentifier.getPortIdentifiers ();
	
	    while (port_list.hasMoreElements ()) {      
	        // Get the list of ports
	        CommPortIdentifier port_id = (CommPortIdentifier) port_list.nextElement ();
	
	        // Find each ports type and name
	        if (port_id.getPortType () == CommPortIdentifier.PORT_SERIAL) 
	            System.out.println ("Serial port: " + port_id.getName ());
	        else if (port_id.getPortType () == CommPortIdentifier.PORT_PARALLEL)
	            System.out.println ("Parallel port: " + port_id.getName ());
	        else
	            System.out.println ("Other port: " + port_id.getName ());
	
	        // Attempt to open it
	        try {
	            CommPort port = port_id.open ("PortListOpen",20);
	            System.out.println ("  Opened successfully");
	            port.close ();
	        } catch  (PortInUseException pe) {
	            System.out.println ("  Open failed");
	            String owner_name = port_id.getCurrentOwner ();
	            if (owner_name == null)
	                System.out.println ("  Port Owned by unidentified app");
	            else
	                // The owner name not returned correctly unless it is
	                // a Java program.
	                System.out.println ("  " + owner_name);
	        }
	   }
	} //main
	
	public String openDrawer() {
	    final byte[] openCD={27,112,0,60,120};
	    String s=new String(openCD);
	    commandSet+=s;
	    return s;
	}


//	public static void main(String args[]) {
//	    PrinterOptions p=new PrinterOptions();
//	    p.openDrawer();
//	    feedPrinter(p.finalCommandSet().getBytes());
//	}


	 private static boolean feedPrinter(byte[] b) {
	    try {
	        AttributeSet attrSet = new HashPrintServiceAttributeSet(new PrinterName("PRINTERNAME", null));
	        //what should I change PRINTERNAME to connect directly to cash drawer
	        DocPrintJob job = PrintServiceLookup.lookupPrintServices(null, attrSet)[0].createPrintJob();
	        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	        Doc doc = new SimpleDoc(b, flavor, null);
	        PrintJobWatcher pjDone = new PrintJobWatcher(job);

	        job.print(doc, null);
	        pjDone.waitForDone();
	        System.out.println("Done !");
	    } catch(javax.print.PrintException pex) {
	        System.out.println("Printer Error " + pex.getMessage());
	        return false;
	    }
	    catch(Exception e) {
	    	e.printStackTrace();
	    	return false;
	    }
	    return true;
	}

	public String finalCommandSet()	{
	    return commandSet;
	}
} // PortListOpen
