package first;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.json.JSONObject;

public class LoginSwingWorker {
	
	public static String Url, AppId, BranchId, Password;
	static JSONObject jsonObject;
	static JSONParser jsonParser;
	static String checksString;
	public static String[] orderIdArray;//, resIdArray;
	
//	public LoginSwingWorker(String url, String appId, String branchId, String password) {
//		this.Url = url;
//		this.AppId = appId;
//		this.BranchId = branchId;
//		this.Password = password;
//	}
	public static void userInfoSave(String url, String appId, String branchId, String password) throws Exception {
		Url = url;
		AppId = appId;
		BranchId = branchId;
		Password = password;
		
		FileWriter fstream = new FileWriter(ReadINIFile.currentPathString + "\\userInfo.properties");
		BufferedWriter out = new BufferedWriter(fstream);
		String info = Url + "/" + AppId + "|" + BranchId + "=" + Password;
		out.write(info);
		out.close();
	}
	public static String userInfoRead() throws Exception {
		String val = null;
		File f = new File(ReadINIFile.currentPathString + "\\userInfo.properties");
		if(f.isFile()) {
			FileInputStream fstream = new FileInputStream(ReadINIFile.currentPathString + "\\userInfo.properties");
			BufferedReader reader = new BufferedReader(new InputStreamReader(fstream));
			StringBuilder sb = new StringBuilder();
			String line;
			while((line = reader.readLine()) != null) {
				sb.append(line);
			}
			val = sb.toString();
		}
		return val;		
	}

	public static void orderAndResInfoDelete(int sel) {
		int allLen = 0;
		if(MainDesign.resultArray != null) { 
			allLen = MainDesign.resultArray.size();
		}
		
		orderIdArray = new String[allLen];
		
		for(int i = 0; i < allLen; i++) {			
			String order_id = MainDesign.resultArray.get(i).getOrderId();
			
			if(order_id != null) {
				orderIdArray[i] = order_id;				
			} else {
				orderIdArray[i] = "0";				
			}
		}
		if(orderIdArray.length > 0) {
			ReadINIFileModel.writeOrderId(orderIdArray);
		} 
		if(sel == 0) {
			int rowCount = MainDesign.tableModel.getRowCount();
			if(rowCount > 0) {
				for(int i = rowCount-1; i > -1; i--) {
					MainDesign.tableModel.removeRow(i);
				}
			}
		} else if(sel == 1) {
			int rowCountOrder = MainDesign.tableModel_order.getRowCount();
			if(rowCountOrder > 0) {
				for(int i = rowCountOrder-1; i > -1; i--) {
					MainDesign.tableModel_order.removeRow(i);
				}
			}
		}
	}
	public static boolean connectCheck() {
		String hostName = "";
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		Socket socket = null;
		boolean reachable = false;
		try {
			try {
				if(socket == null) {
					socket = new Socket(hostName, 80);
					reachable = true;
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
			if(socket != null) {
				try {
					socket.close();
				} catch(IOException e) {					
				}
			}
		}
		return reachable;
	}
}
