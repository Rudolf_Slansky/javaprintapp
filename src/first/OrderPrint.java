package first;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import java.util.Properties;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.Attribute;
import javax.swing.JOptionPane;

import Util.DataConstants;
import Util.Util;


import Util.Printer;
import adapter.ListInformation;
import adapter.NewsItem;
 
public class OrderPrint implements Printable {

	public String _order_id;
	public int _row;
	public int _selectedTab = 0;
	public String[] orderPrinterArray;
	public Paper paper = new Paper();
	public boolean isPrinted = false;
	
	public int pageBreaks;
	public String[] textLines;
	public String o_count;
	public int _finalFlag;
	
	public OrderPrint(String orderId, int tabIndex, int finalFlag) {
		_order_id = orderId;
		_selectedTab = tabIndex;
		_finalFlag = finalFlag;
	}


	public String orderText(String orderId) {
    	
    	ArrayList<NewsItem> orderDetail = new ArrayList<NewsItem>();
		
		orderDetail = ListInformation.detailOrderData(orderId);
		
		String trans_id = orderDetail.get(0).getTransId();
		String order_date = orderDetail.get(0).getOrderDate();
		String order_conf_status = orderDetail.get(0).getOrderConfirmationStatus();
		String print_status = orderDetail.get(0).getPrintingStatus();
		String order_type = orderDetail.get(0).getOrderType();
		String payment_type = orderDetail.get(0).getPaymentType();
		String order_time = orderDetail.get(0).getOrderTime();
		String sub_total = orderDetail.get(0).getSubTotal();
		String customer_id = orderDetail.get(0).getCustomerId();
		
		//================ Version copy ============================
		StringBuffer receiptHeadBuffer = new StringBuffer();		
		
		receiptHeadBuffer.append(Util.center("-----------------------------------     ORDER     -----------------------------------", DataConstants.RECEIPT_WIDTH) + "\n\n;;");		
		
		receiptHeadBuffer.append("Id                 :          " + _order_id + "\n;");
		receiptHeadBuffer.append("Transaction ID     :          " + trans_id + "\n;");
		receiptHeadBuffer.append("Order Date         :          " + order_date + "\n;");
		receiptHeadBuffer.append("Order Status       :          " + order_conf_status + "\n;");
		receiptHeadBuffer.append("Order Type         :          " + order_type + "\n;");
		receiptHeadBuffer.append("Payment Type       :          " + payment_type + "\n;");
		receiptHeadBuffer.append("Order Time         :          " + order_time + "\n;");
		receiptHeadBuffer.append("SubTotal           :          " + sub_total + "\n;");
		receiptHeadBuffer.append("Customer ID        :          " + customer_id + "\n;");
		
		receiptHeadBuffer.append(Util.center("---------------------------------------------------------------------------------------", DataConstants.RECEIPT_WIDTH) + "\n;;");
		receiptHeadBuffer.append(Util.center("-----------------------------   END OF ORDER   ------------------------------", DataConstants.RECEIPT_WIDTH) + "\n;");
		
		
		String receipt = receiptHeadBuffer.toString();
		return receipt;
    }
	@Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex > 0) { /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }
		pageFormat.setPaper(paper);
		
        Graphics2D g2d = (Graphics2D) graphics;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        String receipt = orderText(_order_id);
        String[] bill = receipt.split(";");
        int y = 15;
        Font f = new Font(Font.SANS_SERIF, Font.PLAIN, 11);//12
        graphics.setFont(f);
        //draw each String in a separate line
        for (int i = 0; i < bill.length; i++) {
            graphics.drawString(bill[i], 40, y);
            y = y + 9;//14
        }
        return PAGE_EXISTS;
    }
	
    public void pp() {
    	Properties pro = ReadINIFile.readIt();
    	
		String orderNum = pro.getProperty("orderNum");
		String orderCheck = pro.getProperty("orderCheck");
				
		int orderCount = Integer.parseInt(orderNum);		
		orderPrinterArray = new String[orderCount];
		
        PrinterService ps = new PrinterService();
        
        
        for(int i=0; i<orderCount; i++) {
        	
        	if(orderCheck.equalsIgnoreCase("unselected")) 
				break;
        	
			String orderPrinter = pro.getProperty("orderPrinter"+i);
			orderPrinterArray[i] = orderPrinter;
			final PrintService pss = ps.getCheckPrintService(orderPrinterArray[i].toString());
			
			if(pss == null)
				continue;

			final PrinterJob job = PrinterJob.getPrinterJob();
			try {
				job.setPrintService(pss);
				job.setPrintable(this);
				
			} catch (PrinterException e) {
				e.printStackTrace();
	        }

			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						job.print();
						
						isPrinted = true;
					} catch (PrinterException e) {
						e.printStackTrace();
						isPrinted = false;
					}
					
					while (true) {
						for(Attribute a : pss.getAttributes().toArray()) {
			            	if(a.getName().equalsIgnoreCase("queued-job-count")) {
			            		o_count = a.toString();
			            		break;
			            	}
			            }
						if(Integer.parseInt(o_count) == 0) {
							break;
						}
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					PrintSuccessAndFailed orderP = new PrintSuccessAndFailed(_order_id, 1);
					
					_row = MainDesign.getRowFromId(_order_id, _selectedTab);
					if(_row == -1) {
						BackgroundService.con = true;
						return;
					}
					
					if(Integer.parseInt(o_count) == 0 && isPrinted) {								
						if(orderP.orderPrintSuccess()) {	
							
							PrinterTypeArray.printedOrder.add(_order_id);
							
							if(_selectedTab == 0) {
								MainDesign.table_all.setValueAt("", _row, 10);
							} else if(_selectedTab == 1) {
								MainDesign.table_order.setValueAt("", _row, 10);
							} 
						}
					} else {
						if(orderP.orderPrintFailed()) {
							String sign = "<html><span style=\"color:red; font-size:15px;\">" + "!" + "</span></html>";
							
							if(_selectedTab == 0) {
								MainDesign.table_all.setValueAt(sign, _row, 10);
								MainDesign.table_all.revalidate();
							} else if(_selectedTab == 1) {
								MainDesign.table_order.setValueAt(sign, _row, 10);
							} 
						}
					}
					if(_finalFlag == 1) {
						if(Integer.parseInt(o_count) == 0) {
							BackgroundService.con = true;
						}
					}
				}
	        });		
			t.start();
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
    }
}
