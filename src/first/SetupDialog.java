package first;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.json.JSONException;

public class SetupDialog {
	
	public static JDialog setupDialog;
	public static JTextField s_url;
	public static JTextField s_app_id;
	public static JTextField s_branch_id;
	public static JPasswordField s_password;
	public static JButton but_save;
	
	public static String _url, _app_id, _branch_id, _password, _webUrl; 
	private static JPanel panel;
	private static JLabel titleBox;
	private static JTextField gotoWebUrl;
	/**
	 * @wbp.parser.entryPoint
	 */
	public static String getWebUrl() {
		//webUrl=https\://abc.com
		Properties p = ReadINIFileModel.readWebUrl();
		String webUrl = null;
		if(p != null) 
			webUrl = p.getProperty("webUrl");
		return webUrl;
	}
	public static void createAndShowDialogGUI() {
		setupDialog = new JDialog();
		setupDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setupDialog.setModal(true);
		setupDialog.getContentPane().setBackground(new Color(17, 215, 184));
		setupDialog.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setBackground(new Color(51, 204, 153));
		panel.setBounds(0, 0, 553, 499);
		setupDialog.getContentPane().add(panel);
		panel.setLayout(null);
		
		titleBox = new JLabel("");
		titleBox.setBounds(65, 23, 416, 451);
		TitledBorder titled = new TitledBorder("Set up");
		titled.setTitleColor(Color.WHITE);
		titled.setTitleFont(new Font("Tahoma", Font.BOLD, 16));
		titleBox.setBorder(titled);
		panel.add(titleBox);
		
		s_url = new JTextField();
		s_url.setBounds(119, 81, 300, 33);
		panel.add(s_url);
		s_url.setFont(new Font("Tahoma", Font.PLAIN, 14));
		s_url.setColumns(10);
		
		JLabel lblEnterAppid = new JLabel("Enter AppId");
		lblEnterAppid.setBounds(119, 122, 267, 21);
		panel.add(lblEnterAppid);
		lblEnterAppid.setForeground(Color.WHITE);
		lblEnterAppid.setFont(new Font("Dialog", Font.BOLD, 16));
		
		s_app_id = new JTextField();
		s_app_id.setBounds(119, 146, 300, 33);
		panel.add(s_app_id);
		s_app_id.setFont(new Font("Tahoma", Font.PLAIN, 14));
		s_app_id.setColumns(10);
		
		s_branch_id = new JTextField();
		s_branch_id.setBounds(119, 216, 300, 33);
		panel.add(s_branch_id);
		s_branch_id.setFont(new Font("Tahoma", Font.PLAIN, 14));
		s_branch_id.setColumns(10);
		
		s_password = new JPasswordField();
		s_password.setBounds(119, 283, 300, 33);
		panel.add(s_password);
		s_password.setFont(new Font("Tahoma", Font.PLAIN, 14));
		s_password.setColumns(10);
		
		but_save = new JButton("Save");
		but_save.setBounds(225, 420, 105, 35);
		panel.add(but_save);
		but_save.setForeground(new Color(255, 255, 255));		
		but_save.setSelectedIcon(new ImageIcon("images/alert_ok_but_press.png"));
		but_save.setFont(new Font("Tahoma", Font.BOLD, 14));
		but_save.setBackground(new Color(61, 111, 106));
		but_save.setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		JLabel lblEnterBranchid = new JLabel("Enter BranchId");
		lblEnterBranchid.setBounds(119, 190, 267, 21);
		panel.add(lblEnterBranchid);
		lblEnterBranchid.setForeground(Color.WHITE);
		lblEnterBranchid.setFont(new Font("Dialog", Font.BOLD, 16));
		
		JLabel lblEnterPassword = new JLabel("Enter Password");
		lblEnterPassword.setBounds(119, 258, 267, 21);
		panel.add(lblEnterPassword);
		lblEnterPassword.setForeground(Color.WHITE);
		lblEnterPassword.setFont(new Font("Dialog", Font.BOLD, 16));
		
		JLabel lblNewLabel = new JLabel("Enter Url");
		lblNewLabel.setBounds(119, 55, 267, 21);
		panel.add(lblNewLabel);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 16));
		
		JLabel gotoWeb = new JLabel("Enter Url for Order Management");
		gotoWeb.setForeground(Color.WHITE);
		gotoWeb.setFont(new Font("Dialog", Font.BOLD, 16));
		gotoWeb.setBounds(119, 326, 337, 21);
		panel.add(gotoWeb);
		
		String weburl = null;
		if(getWebUrl() == null)
			weburl = "https://";
		else 
			weburl = getWebUrl();
		gotoWebUrl = new JTextField();
		gotoWebUrl.setText(weburl);
		gotoWebUrl.setFont(new Font("Tahoma", Font.PLAIN, 14));
		gotoWebUrl.setColumns(10);
		gotoWebUrl.setBounds(119, 351, 300, 33);
		panel.add(gotoWebUrl);
		
		but_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				userInfoSetup();
			}
		});
		
		setupDialog.setBackground(new Color(61, 111, 106));
		setupDialog.setSize(558, 528);
		setupDialog.setLocationRelativeTo(null);
		setupDialog.setResizable(false);
		setupDialog.setVisible(true);
	}
	protected static void userInfoSetup() {
		
		_url = s_url.getText().toString();
		_app_id = s_app_id.getText().toString();
		_branch_id = s_branch_id.getText().toString();
		_password = new String(s_password.getPassword());
		_webUrl = gotoWebUrl.getText().trim();
		
		String w = _webUrl.substring(_webUrl.lastIndexOf("/")+1);
		
		if(!w.equals("")) {
			ReadINIFileModel.writeWebUrl(_webUrl);
			setupDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			setupDialog.setVisible(false);
		} 
		
		if(!_url.equals("") && !_app_id.equals("") && !_branch_id.equals("") && !_password.equals("")) {
			
			String Url = _url;
			String AppId  = _app_id; 
			String BranchId = _branch_id;
			String Password = _password;
			
			String query = null;
			try {
				query = URLEncoder.encode("{" +	"\"app\"" +":"+AppId+ "," + "\"password\""+":"+"\""+Password+"\""+"}" ,"utf-8");
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
			String setupUrl = "http://" + Url + "/branches/" + BranchId + "?data=" + query;
			MainDesign.jsonObject = MainDesign.jsonParser.getJSONFromUrl(setupUrl);
			if(MainDesign.jsonObject != null) {
				String checksString = null;
				try {
					checksString = MainDesign.jsonObject.getString("login");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				if(checksString.equalsIgnoreCase("OK")) {
					try {
//						LoginSwingWorker.userInfoDelete();
						LoginSwingWorker.userInfoSave(Url, AppId, BranchId, Password);
					} catch (Exception e) {
						e.printStackTrace();
					}
					setupDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					setupDialog.setVisible(false);
				} else {
					MainDesign.showMessage(setupDialog, "Please enter valid values");
				}
			} else {
				MainDesign.showMessage(setupDialog, "Please enter valid values");
				s_url.selectAll();
				s_url.requestFocus();
			}
			
		} else if(w.equals("") && _url.equals("") && _app_id.equals("") && _branch_id.equals("") && _password.equals("")) {
			MainDesign.showMessage(setupDialog, "Please enter values");
			s_url.selectAll();
			s_url.requestFocus();
		}
		
	}
}
