package first;

import java.awt.*;
import java.awt.event.*;

import javax.print.PrintService;
import javax.swing.*;

import adapter.ListInformation;
import adapter.NewsItem;

import java.awt.print.*;
import java.util.ArrayList;
import java.util.Properties;

public class OrderPrintCheck implements Printable {

    public int[] pageBreaks;
    public String[] textLines;
    public String[] orderPrinterArray;
    public String[] orderPrintTextArray;
    public ArrayList<NewsItem> orderListArray;
    public String[] printLine;
    
    public OrderPrintCheck(ArrayList<NewsItem> orderResultArray) {
    	orderListArray = orderResultArray;
	}
    
    public String[] orderText(ArrayList<NewsItem> orderResultArray) {
    	int len = orderResultArray.size();
    	String[] orderIdArray = new String[len];
    	for(int i=0; i<len; i++) {
    		String order_id = orderResultArray.get(i).getOrderId();
    		boolean isNotPrinted = orderResultArray.get(i).getEMark();
    		if(isNotPrinted)
    			orderIdArray[i] = order_id;
    	}
    	
    	int orderCount = orderIdArray.length;
    	orderPrintTextArray = new String[orderCount];
    			
    	for(int i=0; i<orderCount; i++) {
    		ArrayList<NewsItem> orderDetail = new ArrayList<NewsItem>();
    		orderDetail = ListInformation.detailOrderData(orderIdArray[i]);
    		
    		String trans_id = orderDetail.get(0).getTransId();
    		String order_date = orderDetail.get(0).getOrderDate();
    		String order_conf_status = orderDetail.get(0).getOrderConfirmationStatus();
    		String print_status = orderDetail.get(0).getPrintingStatus();
    		String order_type = orderDetail.get(0).getOrderType();
    		String payment_type = orderDetail.get(0).getPaymentType();
    		String order_time = orderDetail.get(0).getOrderTime();
    		String sub_total = orderDetail.get(0).getSubTotal();
    		String customer_id = orderDetail.get(0).getCustomerId();
    		
    		String receipt = "-------------- ORDER --------------;"
    						+ "Transaction ID     :          " + trans_id + ";"
    						+ "Order Date         :          " + order_date + ";"
    						+ "Order Status       :          " + order_conf_status + ";"
    						+ "Payment Type       :          " + payment_type + ";"
    						+ "Order Time         :          " + order_time + ";"    						
    						+ "SubTotal           :          " + sub_total + ";"    						
    						+ "Customer ID        :          " + customer_id + ";"
    						+ "----------------------------------------------;"
    						+ "----------------------------------------------;";//16 lines
    		orderPrintTextArray[i] = receipt;
    	}
		return orderPrintTextArray;
    }

    public int print(Graphics g, PageFormat pf, int pageIndex) throws PrinterException {

        Font font = new Font("Serif", Font.PLAIN, 10);
        FontMetrics metrics = g.getFontMetrics(font);
        int lineHeight = metrics.getHeight();
        int lineCount = 0;
        String[] printData = orderText(orderListArray);
        int len = printData.length;
        if (pageBreaks == null) {
            for(int i=0; i<len; i++) {
            	String receipt = printData[i];
            	String[] bill = receipt.split(";");
                lineCount = lineCount + bill.length;
            }
            
            int index = 0;
            printLine = new String[lineCount];
            for(int i = 0; i < len; i++) {
            	String receipt = printData[i];
            	String[] bill = receipt.split(";");
            	for(int j = 0; j < bill.length; j++) {
            		String d = bill[j];
            		printLine[index] = d;
            		index++;
            	}
            }
            
            int linesPerPage = (int)(pf.getImageableHeight()/lineHeight);//46
            int numBreaks = (lineCount-1)/linesPerPage;
            pageBreaks = new int[numBreaks];
            for (int b=0; b<numBreaks; b++) {
                pageBreaks[b] = (b+1)*linesPerPage; 
            }
        }
        
        if (pageIndex > pageBreaks.length) {
            return NO_SUCH_PAGE;
        }

        Graphics2D g2d = (Graphics2D)g;
        g2d.translate(pf.getImageableX(), pf.getImageableY());

        int y = 0; 
        int start = (pageIndex == 0) ? 0 : pageBreaks[pageIndex-1];
        int end   = (pageIndex == pageBreaks.length) ? lineCount : pageBreaks[pageIndex];
        for (int k=start; k<end; k++) {
            
//            for(int j=0; j<printData.length; j++) {
//            	String[] bill = printData[j].split(";");
//            	for(int k=0; k<bill.length; k++) {
            		y += lineHeight;
            		g.drawString(printLine[k], 0, y);
//            		System.out.println(printLine[k]);
//            	}
//            }
            
        }

        return PAGE_EXISTS;
    }
    public void pp() {
    	Properties pro = ReadINIFile.readIt();
		String orderNum = pro.getProperty("orderNum");
		String orderCheck = pro.getProperty("orderCheck");
		final String isAlert = pro.getProperty("alertCheck");
		int orderCount = Integer.parseInt(orderNum);
		
		orderPrinterArray = new String[orderCount];
		
        PrinterService ps = new PrinterService();        
        
        for(int i=0; i<orderCount; i++) {
        	if(orderCheck.equalsIgnoreCase("unselected")) 
				break;
			
			String orderPrinter = pro.getProperty("orderPrinter"+i);
			orderPrinterArray[i] = orderPrinter;
			final PrintService pss = ps.getCheckPrintService(orderPrinterArray[i].toString());
			
			if(pss == null)
				continue;
			final PrinterJob job = PrinterJob.getPrinterJob();
			try {
				Book book = new Book();
				book.append(this,  job.defaultPage());
				job.setPageable(book);
				job.setPrintService(pss);
				job.setPrintable(this);
			} catch (PrinterException e) {
				e.printStackTrace();
	        }			
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
//						if(isAlert.equalsIgnoreCase("selected")) {
//							AudioPlayer a = new AudioPlayer();
//							a.playBack();
//						}
						job.print();
					} catch (PrinterException e) {
						e.printStackTrace();
					}
				}
	        }).start();
			
        }
    }
}
