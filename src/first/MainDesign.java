package first;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractButton;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import org.json.JSONException;
import org.json.JSONObject;

//import com.teamdev.jxbrowser.chromium.Browser;
//import com.teamdev.jxbrowser.chromium.swing.BrowserView;

import adapter.ListInformation;
import adapter.NewsItem;

public class MainDesign {
	public static JFrame frame_login;
	public static RoundedPanel panel_login;
	public static RoundedPanel panel_welcome;
	public static JTextField enter_url;
	public static JTextField enter_app_id;
	public static JTextField enter_branch_id;
	public static JPasswordField enter_password;
	public static Image img;
	public static JLabel lblNewLabel_1;
	public static JLabel imgLabel;
	public static JButton but_login;
	
	public static String url;
	public static String appId;
	public static String branchId;
	public static String password;
	
	public static JSONObject jsonObject, jsonImgObject;
	public static JSONParser jsonParser;
	public static String checksString;
	public static ArrayList<NewsItem> resultArray;
	public static ArrayList<NewsItem> orderResultArray;
	
	
	public static JFrame frame;
	public static JTextField textField_lastName;
	public static JTextField textField_systemId;
	public static JPanel panel_left;
	public static JPanel panel_top;
	public static JPanel panel_search;
	public static JPanel panel_main;
	public static JLabel circleLabel;
	public static JLabel markImageLabel;
	public static JTabbedPane tabbedPane;
	public static JButton but_setup, but_select_printer, but_check, but_print, but_history, but_search, but_goto;
	public static JToggleButton toggle_auto;
	
	public static JTable table_all, table_order;//, table_res;
	public static DefaultTableModel tableModel, tableModel_order;//, tableModel_res;
	public static JScrollPane scrollPane;
	public static boolean DEBUG = false;

    public static JPanel panel_detail = new JPanel();
    public static Integer indexer = 1;
    public static List<JLabel> listOfLabels = new ArrayList<JLabel>();
    public static List<JTextField> listOfTextFields = new ArrayList<JTextField>();
	public static JScrollPane scrollpane_detail;
	
	public static String order_id;
	
	public static String[] orderArray;
	public static int row;
	public static int selectedTab = 0;

	public static DefaultTableCellRenderer cr = new DefaultTableCellRenderer();
	
	// computer windows size getting
	public static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	public static int screenH, screenW, w, h, leftW, leftH, mainH, mainW, topW, topH, start_x = 10, start_y = 10, but_h = 60;
	public static String autoCheck;
	public static boolean preAutoFlag = false;
	public static boolean manualPrintFlag = false;
	
	public static ImageIcon titleIcon = new ImageIcon(MainDesign.class.getResource("/resources/print_icon3.png"));
	public static String sign = "<html><span style=\"color:red; font-size:15px;\">" + "!" + "</span></html>";
	public static String notsign = "<html><span style=\"color:green; font-size:15px;\">" + "&#10004;" + "</span></html>";
	
	
	public static Timer serverTimer;
	public static ServerTimerTask serverTimerTask;
	public static int period = 10000000;
	public static JTextField gotoWebText;
	public static String _webUrl;
	
	public static Object obj = new Object();
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void createAndShowGUI() {
		jsonParser = new JSONParser();
		int a, b, c;
		screenW = (int)screenSize.getWidth();
		screenH = (int)screenSize.getHeight() - 40;
		
		File f_ = new File(ReadINIFile.currentPathString + "\\temp.properties");
		File f_user = new File(ReadINIFile.currentPathString + "\\userInfo.properties");
		File f_auto = new File(ReadINIFile.currentPathString + "\\tempAuto.properties");
		File f_model = new File(ReadINIFile.currentPathString + "\\tempModel.properties");
		File f_web = new File(ReadINIFile.currentPathString + "\\tempWeb.properties");
		File f_order = new File(ReadINIFile.currentPathString + "\\tempOrder.properties");
		try {
			if(!f_.isFile())
				f_.createNewFile();
			if(!f_user.isFile())
				f_user.createNewFile();
			if(!f_auto.isFile())
				f_auto.createNewFile();
			if(!f_model.isFile())
				f_model.createNewFile();
			if(!f_web.isFile())
				f_web.createNewFile();
			if(!f_order.isFile())
				f_order.createNewFile();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		try {
			autoRun();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			String info = LoginSwingWorker.userInfoRead();
			if(info == null) {
				loginFrameShow();
			} else {
				if(!info.equals("")) {
					a = info.lastIndexOf("/");
					b = info.indexOf("|");
					c = info.indexOf("=");
					url = info.substring(0, a);//url
					appId = info.substring(a+1, b);//offset
					branchId = info.substring(b+1, c);//branchId
					password = info.substring(c+1);//limit
					mainFrameShow();
				} else {
					loginFrameShow();
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	public static String getAutoStart() {
		return System.getProperty("java.io.tmpdir").replace("Local\\Temp\\", "Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup");
	}
	public static String getRunningDir() {
		String runDir = Paths.get(".").toAbsolutePath().normalize().toString();
		return runDir;
	}
	private static void autoRun() throws Exception {
		
		Path startupPath = Paths.get(getAutoStart());
		Path currentPath = Paths.get(getRunningDir());
				
		String batFilePath = startupPath.toString() + "\\AutoRun.bat";
		String batCommand = "\"" + currentPath.toString() + "\\AppOrderProV3.exe" + "\"";
		File f = new File(batFilePath);
		if(f.isFile()) {
			return;
		}
		FileWriter batStream = new FileWriter(batFilePath);
		BufferedWriter out = new BufferedWriter(batStream);
		out.write(batCommand);
		out.close();
	}
	
	//==========  main screen  =================================================================
	public static void mainFrameShow() {
		
		//table's cell Value is align on Center of cell
		cr.setHorizontalAlignment(JLabel.CENTER);
		
		resultArray = ListInformation.listInfo();
		orderResultArray = ListInformation.orderInfo();
		
		ImageIcon setIcon = new ImageIcon(MainDesign.class.getResource("/resources/setup_icon1.png"));		
		ImageIcon reprintIcon = new ImageIcon(MainDesign.class.getResource("/resources/reprinter_icon1.png"));
		ImageIcon checkIcon = new ImageIcon(MainDesign.class.getResource("/resources/check_icon1.png"));
		ImageIcon autocheckIcon = new ImageIcon(MainDesign.class.getResource("/resources/autocheck_icon1.png"));
		ImageIcon printIcon = new ImageIcon(MainDesign.class.getResource("/resources/print_title_icon1.png"));
		ImageIcon delecteIcon = new ImageIcon(MainDesign.class.getResource("/resources/delete_history_icon2.png"));
		ImageIcon arrowIcon = new ImageIcon(MainDesign.class.getResource("/resources/arrow.png"));
		
		
		//main frame;
			frame = new JFrame("Order Print Application");
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);			
			frame.getContentPane().setBackground(new Color(17, 215, 184));
			frame.getContentPane().setLayout(null);
			frame.setResizable(true);
			frame.setLocation(0, 0);			
			frame.setSize(screenW, screenH);
			frame.setIconImage(titleIcon.getImage());
			frame.setVisible(true);
			
		//left empty panel
			panel_left = new JPanel();
			panel_left.setBackground(new Color(17, 215, 184));
			leftW = 160;
			leftH = screenH;
			panel_left.setBounds(0, 0, leftW, leftH);
			frame.getContentPane().add(panel_left);
			panel_left.setLayout(null);
			
			Image img = GetMarkImage.getMarkImage();
			if(img != null) {
				markImageLabel = new JLabel(new ImageIcon(img));
				markImageLabel.setText("");
				markImageLabel.setHorizontalTextPosition(JLabel.CENTER);
				markImageLabel.setVerticalTextPosition(JLabel.CENTER);
				markImageLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
				markImageLabel.setBounds(15, 15, 120, 120);
				markImageLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
				markImageLabel.setToolTipText("Minimize");
				panel_left.add(markImageLabel);
			}
//		
		//top button deploy
			panel_top = new JPanel();
			panel_top.setBackground(new Color(17, 215, 184));
			topW = screenW - 170;
			topH = 70;
			panel_top.setBounds(160, 15, topW, topH);
			frame.getContentPane().add(panel_top);
			panel_top.setLayout(null);
			
		//set up Button
			but_setup = new JButton("Set up");			
			int but_setup_w = 70;
			but_setup.setBounds(start_x, start_y, but_setup_w, but_h);
			panel_top.add(but_setup);
			but_setup.setToolTipText("Set up");
			but_setup.setVerticalTextPosition(AbstractButton.BOTTOM);
			but_setup.setHorizontalTextPosition(AbstractButton.CENTER);
			but_setup.setMnemonic(KeyEvent.VK_M);
			but_setup.setIcon(setIcon);
			but_setup.setCursor(new Cursor(Cursor.HAND_CURSOR));
			but_setup.setFocusCycleRoot(true);
			
		//top - Printers button
			but_select_printer = new JButton("Printers");
			int but_select_printer_w = 80;
			int but_select_printer_x = start_x + but_setup_w + 5;
			but_select_printer.setBounds(but_select_printer_x, start_y, but_select_printer_w, but_h);
			panel_top.add(but_select_printer);
			but_select_printer.setToolTipText("Printer Select");
			but_select_printer.setVerticalTextPosition(AbstractButton.BOTTOM);
			but_select_printer.setHorizontalTextPosition(AbstractButton.CENTER);
			but_select_printer.setMnemonic(KeyEvent.VK_M);
			but_select_printer.setIcon(reprintIcon);
			but_select_printer.setCursor(new Cursor(Cursor.HAND_CURSOR));
			but_select_printer.setFocusCycleRoot(true);
			
		//top - Check button
			but_check = new JButton("All Print");
			int but_check_w = 80;
			int but_check_x = but_select_printer_x + but_select_printer_w + 5; 
			but_check.setBounds(but_check_x, start_y, but_check_w, but_h);
			panel_top.add(but_check);
			but_check.setToolTipText("All Print");
			but_check.setVerticalTextPosition(AbstractButton.BOTTOM);
			but_check.setHorizontalTextPosition(AbstractButton.CENTER);
			but_check.setMnemonic(KeyEvent.VK_M);
			but_check.setIcon(checkIcon);
			but_check.setCursor(new Cursor(Cursor.HAND_CURSOR));
			but_check.setFocusCycleRoot(true);
			
		//top - AutoCheck button
			toggle_auto = new JToggleButton("Auto Print");
			int toggle_auto_w = 90;
			int toggle_auto_x = but_check_x + but_check_w + 5;
			toggle_auto.setBounds(toggle_auto_x, start_y, toggle_auto_w, but_h);
			panel_top.add(toggle_auto);
			toggle_auto.setToolTipText("Auto Print");
			toggle_auto.setVerticalTextPosition(AbstractButton.BOTTOM);
			toggle_auto.setHorizontalTextPosition(AbstractButton.CENTER);
			toggle_auto.setMnemonic(KeyEvent.VK_M);
			toggle_auto.setIcon(autocheckIcon);
			toggle_auto.setCursor(new Cursor(Cursor.HAND_CURSOR));
			toggle_auto.setFocusCycleRoot(true);
			
		//===================== Circle Progress Showing ==================================
			ImageIcon circleIcon = new ImageIcon(MainDesign.class.getResource("/resources/circle.GIF"));
			circleLabel = new JLabel();
			circleLabel.setBounds(screenW - 300, 0, 100, 100);
			circleLabel.setBackground(new Color(17, 215, 184));
			circleLabel.setIcon(circleIcon);
			circleLabel.setVisible(false);
			panel_top.add(circleLabel);
			
		//top - Print  button
			but_print = new JButton("Manual Print");
			int but_print_w = 110;
			int but_print_x = toggle_auto_x + toggle_auto_w + 5;
			but_print.setBounds(but_print_x, start_y, but_print_w, but_h);
			panel_top.add(but_print);
			but_print.setToolTipText("Manual Print");
			but_print.setVerticalTextPosition(AbstractButton.BOTTOM);
			but_print.setHorizontalTextPosition(AbstractButton.CENTER);
			but_print.setMnemonic(KeyEvent.VK_M);
			but_print.setIcon(printIcon);
			but_print.setCursor(new Cursor(Cursor.HAND_CURSOR));
			but_print.setFocusCycleRoot(true);
			
		//top - DeleteHistory button
			but_history = new JButton("Delete History");
			int but_history_w = 120;
			int but_history_x = but_print_x + but_print_w + 5;
			but_history.setBounds(but_history_x, start_y, but_history_w, but_h);
			panel_top.add(but_history);
			but_history.setToolTipText("Delete History");
			but_history.setVerticalTextPosition(AbstractButton.BOTTOM);
			but_history.setHorizontalTextPosition(AbstractButton.CENTER);
			but_history.setMnemonic(KeyEvent.VK_M);
			but_history.setIcon(delecteIcon);
			but_history.setCursor(new Cursor(Cursor.HAND_CURSOR));
			
		//top - goto website button
			but_goto = new JButton("Go To Order Management");
			int but_goto_w = 200;
			int but_goto_x = but_history_x + but_history_w + 5;
			but_goto.setBounds(but_goto_x, 30, but_goto_w, 40);
			panel_top.add(but_goto);
			but_goto.setToolTipText("Go To Order Management");
			but_goto.setVerticalTextPosition(AbstractButton.CENTER);
			but_goto.setHorizontalTextPosition(AbstractButton.LEFT);
			but_goto.setMnemonic(KeyEvent.VK_M);
			but_goto.setIcon(arrowIcon);
			but_goto.setCursor(new Cursor(Cursor.HAND_CURSOR));
			
			but_goto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					GotoWebSite.createAndShowDialogGUI();		
					
				}
			});
			but_history.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {					
					DeleteDialog.createAndShowDialogGUI();
				}
			});
			but_print.addActionListener(new ActionListener() {				
				public void actionPerformed(ActionEvent arg0) {
					
					Properties pro = ReadINIFile.readIt();
					if(pro == null || pro.getProperty("orderNum") == null || pro.getProperty("orderCheck") == null) {
						JOptionPane.showMessageDialog(null, "Please select printer in Printers");
						return;
					}
					
					new Thread(new Runnable() {
						@Override
						public void run() {	
							manualPrintFlag = true;
							if(selectedTab == 0) {								
									final int[] selectedIndex = table_all.getSelectedRows();
									if(selectedIndex.length == 0) {
										JOptionPane.showMessageDialog(null, "Please select items that you want to print in below list");
										return;
									}
									preAutoFlag = false;
									if(ReadINIFileAuto.readIt() != null && ReadINIFileAuto.readIt().equalsIgnoreCase("ok")) {//autoCheck is running 
										preAutoFlag = true;
										autoCheckStop();
										toggle_auto.setSelected(false);
									}
									showCircleProgress();
									but_print.setEnabled(false);
									
									for(int i = 0; i <table_all.getSelectedRowCount(); i++) {
										String _id = (String)table_all.getValueAt(selectedIndex[i], 1);
										OrderPrint o = new OrderPrint(_id, selectedTab, 0);
										o.pp();
									}	
							} else if(selectedTab == 1) {								
									final int[] selectedIndex = table_order.getSelectedRows();
									if(selectedIndex.length == 0) {
										JOptionPane.showMessageDialog(null, "Please select items that you want to print in below list");
										return;
									}
							
									preAutoFlag = false;
									if(ReadINIFileAuto.readIt() != null && ReadINIFileAuto.readIt().equalsIgnoreCase("ok")) {//autoCheck is running 
										preAutoFlag = true;
										autoCheckStop();
										toggle_auto.setSelected(false);
									}
									showCircleProgress();
									but_print.setEnabled(false);
									
									for(int i = 0; i <table_order.getSelectedRowCount(); i++) {			
										String _id = (String)table_order.getValueAt(selectedIndex[i], 1);
										OrderPrint o = new OrderPrint(_id, selectedTab, 0);
										o.pp();										
									}		
							}
							hideCircleProgress();
							but_print.setEnabled(true);
							if(preAutoFlag) {								
								toggle_auto.setSelected(true);
								autoCheck("autoCheck", selectedTab);
								preAutoFlag = false;
							}
							manualPrintFlag = false;
						}
						
					}).start();
				}
			});
			
//			markImageLabel.addMouseListener(new MouseListener() {
//				@Override
//				public void mouseClicked(MouseEvent arg0) {
//					frame.setState(JFrame.ICONIFIED);
//				}
//				@Override
//				public void mouseEntered(MouseEvent arg0) {}
//				@Override
//				public void mouseExited(MouseEvent arg0) {}
//				@Override
//				public void mousePressed(MouseEvent arg0) {}
//				@Override
//				public void mouseReleased(MouseEvent arg0) {}
//				
//			});
			
			String autoChk = ReadINIFileAuto.readIt();
			if(autoChk != null) {
				if(autoChk.equalsIgnoreCase("Ok")) {
					toggle_auto.setSelected(true);
					autoCheck("autoCheck", 0);
				} else {
					toggle_auto.setSelected(false);
				}
			}
			toggle_auto.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent event) {
					if(event.getStateChange() == ItemEvent.SELECTED) {
						ReadINIFileAuto.writeIt("Ok");
						autoCheck("autoCheck", selectedTab);
					} else if(event.getStateChange() == ItemEvent.DESELECTED) {
						ReadINIFileAuto.writeIt("Cancel");
						autoCheckStop();
					}
				}				
			});
			but_check.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					but_check.setEnabled(false);
					autoCheck("check", selectedTab);
				}
			});
			
			but_select_printer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					SelectPrinterDialog.createAndShowDialogGUI();
				}
			});
			but_setup.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					SetupDialog.createAndShowDialogGUI();
				}
			});
			
		//center Main Panel
			panel_main = new JPanel();
			panel_main.setBackground(new Color(255, 255, 255));
			
			Rectangle mainRect = frame.getBounds();
			mainH = mainRect.height;
			mainW = mainRect.width;
			panel_main.setBounds(160, 150, mainW-leftW, mainH-topH);
			panel_main.setLayout(null);
			frame.getContentPane().add(panel_main);
			
		//Tab Pane
			tabbedPane = new JTabbedPane();
			tabbedPane.setBackground(new Color(255, 255, 255));
			Rectangle r = panel_main.getBounds();
			h = r.height;
			w = r.width-5;			
			tabbedPane.setBounds(1, 0, w, h-111);
			
		//"View All" Tab Data	
			tableAllShow(resultArray);
		//"Orders" Tab Data == Table Data
//			tableOrderShow(orderResultArray);
			
			tabbedPane.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent event) {
					selectedTab = tabbedPane.getSelectedIndex();
					if(selectedTab == 0) {
						resultArray = ListInformation.listInfo();
						viewAllShow(resultArray, tableModel, 1);
					} else if(selectedTab == 1) {
						orderResultArray = ListInformation.orderInfo();
						viewAllShow(orderResultArray, tableModel_order, 2);
					}
				}
				
			});
			panel_main.add(tabbedPane);
			
		//Search Panel
			panel_search = new JPanel();
			panel_search.setBackground(new Color(17, 215, 184));
			panel_search.setBounds(271, 92, 727, 50);
			panel_search.setLayout(null);
			frame.getContentPane().add(panel_search);
			
			
		//Text first field in Search Panel
			textField_lastName = new JTextField();
			textField_lastName.setToolTipText("Id / CustomerID");
			textField_lastName.setColumns(10);
			textField_lastName.setBackground(Color.WHITE);
			textField_lastName.setBounds(5, 15, 170, 30);
			panel_search.add(textField_lastName);
			
			but_search = new JButton("Search");
			but_search.setBounds(200, 15, 100, 30);
			but_search.setCursor(new Cursor(Cursor.HAND_CURSOR));
			panel_search.add(but_search);
									
			but_search.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					String searchVal = textField_lastName.getText().toString();				
					int selectedTab = tabbedPane.getSelectedIndex();
					JScrollPane scrollPane = (JScrollPane)tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
					JTable table = (JTable)scrollPane.getViewport().getView();
			
					if(searchVal.equals("")) {
						showMessage(frame, "Please enter key for searching");
						textField_lastName.requestFocus();
					} else {
						if(selectedTab == 0) {
							searchListShow(tableModel, searchVal);
						} else if(selectedTab == 1) {
							searchListShow(tableModel_order, searchVal);
						} 
					}
				}
			});
			textField_lastName.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {					
					String searchVal = textField_lastName.getText().toString();					
					int selectedTab = tabbedPane.getSelectedIndex();
					JScrollPane scrollPane = (JScrollPane)tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
					JTable table = (JTable)scrollPane.getViewport().getView();
					if(searchVal.equals("")) {
						showMessage(frame, "Please enter key for searching");
						textField_lastName.requestFocus();
					} else {
						if(selectedTab == 0) {
							searchListShow(tableModel, searchVal);
						} else if(selectedTab == 1) {
							searchListShow(tableModel_order, searchVal);
						}
					}
				}
				
			});
			textField_lastName.getDocument().addDocumentListener(new DocumentListener() {
				@Override
				public void changedUpdate(DocumentEvent arg0) {}
				@Override
				public void insertUpdate(DocumentEvent arg0) {}
				@Override
				public void removeUpdate(DocumentEvent arg0) {
					recoveryShow();
				}				
			});
		
	}	
	
	public static void autoCheckStop() {
		BackgroundService bs = new BackgroundService();
		bs.autoCheckStop();
		hideCircleProgress();
	}
	public static void autoCheck(String flag, int selTabIndex) {	
		Properties pro = new Properties();
		File f = new File(ReadINIFile.currentPathString + "\\temp.properties");
		if(f.isFile()) {				
			try {
				pro.load(new FileInputStream(ReadINIFile.currentPathString + "\\temp.properties"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			String o_chk_val = pro.getProperty("orderCheck");
			
			if(o_chk_val == null)
				o_chk_val = "unselected";
			if(o_chk_val.equals("unselected")) {
				String msg = "<html>1. Please select Printer in the Printers.<br>2. Please check the CheckBox for order in the Printers.</html>";
				showMessage(frame, msg);
				return;
			}
		}
		showCircleProgress();
		BackgroundService bs = new BackgroundService();
		bs.autoPrint(flag, selTabIndex);
	}
	
	private static void tableOrderShow(final ArrayList<NewsItem> orderResultArray) {
		Object[] columnNames_order = {"No", "Id", "Transaction ID", "Order Date", "Order Status", "Order Type", "Payment Type", "Order Time", "SubTotal", "Customer ID", "Print Status", ""};
		tableModel_order = new DefaultTableModel(columnNames_order, 0);	
		
		viewAllShow(orderResultArray, tableModel_order, 2);
		table_order = new JTable(tableModel_order) {
			public boolean isCellEditable(int rowIndex, int vColIndex) {
				return false;
			}			
		};
				
		table_order.setPreferredScrollableViewportSize(new Dimension(w-20, h-111));
		table_order.setFillsViewportHeight(true);
		table_order.setBounds(0, 0, 0, 0);
		table_order.setRowHeight(30);
		
		table_order.setSelectionModel(new DefaultListSelectionModel() {
			@Override
			public void setSelectionInterval(int index0, int index1) {
				if(super.isSelectedIndex(index0))
					super.removeSelectionInterval(index0, index1);
				else {
					super.addSelectionInterval(index0, index1);
				}
			}
		});
		
		for(int i = 0; i < columnNames_order.length; i++) {
			table_order.getColumnModel().getColumn(i).setCellRenderer(cr);
		}
		if (DEBUG) {
			table_order.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
				    printDebugData(table_order);
				}
			});
		}
		scrollPane = new JScrollPane(table_order);
		ImageIcon orderIcon = new ImageIcon(MainDesign.class.getResource("/resources/order_icon1.png"));
		tabbedPane.addTab("Orders", orderIcon, scrollPane, null);
		
	}
	public static void tableAllShow(final ArrayList<NewsItem> resultArray) {
		Object[] columnNames = {"No", "Id", "Transaction ID", "Order Date", "Order Status", "Order Type", "Payment Type", "Order Time", "SubTotal", "Customer ID", "Print Status", ""};
		tableModel = new DefaultTableModel(columnNames, 0);	
		tableModel.setDataVector(new Object[][]{{"button1"}}, columnNames);
		viewAllShow(resultArray, tableModel, 1);
		table_all = new JTable(tableModel) {
			public boolean isCellEditable(int rowIndex, int vColIndex) {
				return false;
			}			
		};
		table_all.setPreferredScrollableViewportSize(new Dimension(w-20, h-111));		
		table_all.setFillsViewportHeight(true);
		table_all.setBounds(0, 0, 0, 0);
		table_all.setRowHeight(30);
		
//		table_all.setSelectionModel(new DefaultListSelectionModel() {
//			@Override
//			public void setSelectionInterval(int index0, int index1) {
//				if(super.isSelectedIndex(index0))
//					super.removeSelectionInterval(index0, index1);
//				else {
//					super.addSelectionInterval(index0, index1);
//				}
//			}
//		});
		
		for(int i = 0; i < columnNames.length; i++) {			
			table_all.getColumnModel().getColumn(i).setCellRenderer(cr);
		}
//		
//		if (DEBUG) {
//			table_all.addMouseListener(new MouseAdapter() {
//				public void mouseClicked(MouseEvent e) {
//				    printDebugData(table_all);
//				}
//			});
//		}
		scrollPane = new JScrollPane(table_all);
		ImageIcon viewAllIcon = new ImageIcon(MainDesign.class.getResource("/resources/view_all_icon.png"));
		tabbedPane.addTab("View All", viewAllIcon, scrollPane, null);
		
	}
	
	protected static void checkOrderAndRes(int index) {

		if(index == 0) {
			if(orderResultArray != null){
				OrderPrintCheck o = new OrderPrintCheck(orderResultArray);
				o.pp();
			}
		} else if(index == 1) {
			if(orderResultArray != null) {
				OrderPrintCheck orderChk = new OrderPrintCheck(orderResultArray);
				orderChk.pp();
			}
		}
	}
	protected static void recoveryShow() {
		if(textField_lastName.getText().toString().equals("")) {
			int selectedTab = tabbedPane.getSelectedIndex();
			JScrollPane scrollPane = (JScrollPane)tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
			JTable table = (JTable)scrollPane.getViewport().getView();
			addListShow(table, selectedTab);
		}
	}
	private static void addListShow(JTable table, int selectedTab) {
		DefaultTableModel model = (DefaultTableModel)table.getModel();
		model.setRowCount(0);
		if(selectedTab == 0)
			viewAllShow(resultArray, model, 1);
		else if(selectedTab == 1)
			viewAllShow(orderResultArray, model, 2);
		table.setModel(model);
	}
	public static void searchListShow(DefaultTableModel tableModel, String searchVal) {
		int rowCount = tableModel.getRowCount();
		if(rowCount > 0) {
			for(int i = rowCount-1; i > -1; i--) {
				String _id = (String)tableModel.getValueAt(i, 1);
				String customer_id = (String)tableModel.getValueAt(i, 9);

				if(_id.equals(searchVal) || customer_id.equalsIgnoreCase(searchVal))
					continue;
				else 
					tableModel.removeRow(i);
			}
		}		
	}
	public static void viewAllShow(final ArrayList<NewsItem> resultArray, final DefaultTableModel tableModel, final int flag) {
				if(serverTimer != null) {
					serverTimer.cancel();
					serverTimer = null;						
				}
				try {
					System.out.print(resultArray);
					if(resultArray != null) {			
						if(tableModel.getRowCount() > 0) {
							for(int j = tableModel.getRowCount()-1; j>-1; j--){
								tableModel.removeRow(j);
							}
						}
						
						String[] orderArr = new String[resultArray.size()];
						
						for(int i=0; i<resultArray.size(); i++) {				
							String orderId = resultArray.get(i).getOrderId();
							if(orderId != null)
								orderArr[i] = orderId;
							else 
								orderArr[i] = "0";
						}
						
						
						for(int i=0; i<resultArray.size(); i++) {				
							String orderId = orderArr[i];
							
							Properties prop_order = ReadINIFileModel.readOrderId();
							
							if(!prop_order.isEmpty()) {
								int maxVal = Integer.parseInt((String)prop_order.getProperty("orderBig"));
								int ordVal = Integer.parseInt(orderId); 
								if(maxVal >= ordVal) {
									orderArr[i] = "0";
								}
							}
							
						}
						
						int k = 1;
						for(int i = 0; i < resultArray.size(); i++) {
							String orderId = orderArr[i];
							if(orderId.equals("0"))
								continue;
							
							if(orderId != null && !orderId.equals("0")){
								String type = "<html><span style=\"color:green;\">" + "Order" + "</span></html>";
								String trans_id = resultArray.get(i).getTransId();
								String order_date = resultArray.get(i).getOrderDate();
								String order_conf_status = resultArray.get(i).getOrderConfirmationStatus();
								String print_status = resultArray.get(i).getPrintingStatus();
								String order_type = resultArray.get(i).getOrderType();
								String payment_type = resultArray.get(i).getPaymentType();
								String order_time = resultArray.get(i).getOrderTime();
								String sub_total = resultArray.get(i).getSubTotal();
								String customer_id = resultArray.get(i).getCustomerId();
								
								boolean isPrinted = resultArray.get(i).getEMark();						
								String num = ""+k;
								if(flag == 1) {
									Object[] orderData = {num, orderId, trans_id, order_date, order_conf_status, order_type, payment_type, order_time, sub_total, customer_id, isPrinted ? sign : "", "View Order"};
									tableModel.addRow(orderData);
								} else if(flag == 2) {
									Object[] orderData = {num, orderId, trans_id, order_date, order_conf_status, order_type, payment_type, order_time, sub_total, customer_id, isPrinted ? sign : "", "View Order"};
									tableModel.addRow(orderData);
								}
							} 
							k++;
						}
					} else {
					}		
					
					//==== server always monitor ==========
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					serverTimer = new Timer();							
					serverTimerTask = new ServerTimerTask();
					serverTimer.schedule(serverTimerTask, 1000, period);
					return;
				} catch (NumberFormatException io) {
					io.printStackTrace();
				} 
	}
	// ===========   Login screen   ================================================
	public static void loginFrameShow() {		
		
		cr.setHorizontalAlignment(JLabel.CENTER);
		
		frame_login = new JFrame("AppOrderPro");
		frame_login.getContentPane().setBackground(new Color(17, 215, 184));
		frame_login.getContentPane().setLayout(null);
		frame_login.setResizable(true);
		frame_login.setIconImage(titleIcon.getImage());
		
		panel_login = new RoundedPanel();
		panel_login.setBackground(new Color(61, 111, 106));
		int loginW = 750;
		int loginH = 480;				
		int loginX = (screenW - loginW) / 2;
		int loginY = (screenH - loginH) / 2 + 50;
		panel_login.setBounds(loginX, loginY, loginW, loginH);
		frame_login.getContentPane().add(panel_login);
		panel_login.setLayout(null);
				
		JLabel lblNewLabel = new JLabel("Enter Url");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBounds(200, 50, 260, 22);
		panel_login.add(lblNewLabel);
		
		JLabel lblEnterAppId = new JLabel("Enter App ID");
		lblEnterAppId.setForeground(Color.WHITE);
		lblEnterAppId.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEnterAppId.setBounds(200, 113, 260, 22);
		panel_login.add(lblEnterAppId);
		
		JLabel lblEnterBranchId = new JLabel("Enter Branch ID");
		lblEnterBranchId.setForeground(Color.WHITE);
		lblEnterBranchId.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEnterBranchId.setBounds(200, 177, 260, 22);
		panel_login.add(lblEnterBranchId);
		
		JLabel lblEnterPassword = new JLabel("Enter Password");
		lblEnterPassword.setForeground(Color.WHITE);
		lblEnterPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEnterPassword.setBounds(200, 245, 260, 22);
		panel_login.add(lblEnterPassword);
		
		enter_url = new JTextField();
		enter_url.setBounds(200, 71, 303, 31);
		panel_login.add(enter_url);
		enter_url.setColumns(10);
		
		enter_app_id = new JTextField();
		enter_app_id.setColumns(10);
		enter_app_id.setBounds(200, 135, 303, 31);
		panel_login.add(enter_app_id);
		
		enter_branch_id = new JTextField();
		enter_branch_id.setColumns(10);
		enter_branch_id.setBounds(200, 199, 303, 31);
		panel_login.add(enter_branch_id);
		
		enter_password = new JPasswordField();
		enter_password.setColumns(10);
		enter_password.setBounds(200, 267, 303, 31);
		panel_login.add(enter_password);
		
		but_login = new JButton("Login");
		but_login.setBounds(294, 396, 119, 39);
		but_login.setFont(new Font("Tahoma", Font.BOLD, 16));
		but_login.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panel_login.add(but_login);
		
		JLabel gotoWeb = new JLabel("Enter Url for Orders and Reservations Management");
		gotoWeb.setForeground(Color.WHITE);
		gotoWeb.setFont(new Font("Tahoma", Font.BOLD, 14));
		gotoWeb.setBounds(200, 315, 360, 22);
		panel_login.add(gotoWeb);
		
		String weburl = null;
		if(getWebUrl() == null)
			weburl = "http://";
		else 
			weburl = getWebUrl();
		gotoWebText = new JTextField();
		gotoWebText.setText(weburl);
		gotoWebText.setColumns(10);
		gotoWebText.setBounds(200, 339, 303, 31);
		panel_login.add(gotoWebText);
		
		but_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {					
					loginConfirm();
				} catch (UnsupportedEncodingException | JSONException e) {
					e.printStackTrace();
				}
			}
		});
		
		panel_welcome = new RoundedPanel();
		panel_welcome.setBackground(new Color(61, 111, 106));
		int welcomeW = loginW;
		int welcomeH = 90;
		int welcomeX = loginX;
		int welcomeY = loginY - welcomeH - 20;
		panel_welcome.setBounds(welcomeX, welcomeY, welcomeW, welcomeH);
		frame_login.getContentPane().add(panel_welcome);
		panel_welcome.setLayout(new BorderLayout());
		
		lblNewLabel_1 = new JLabel();
		String sb = "<html>&nbsp;&nbsp;&nbsp;&nbsp;Please enter the criteria that you will find in your \"Branch Print Report\"<br>&nbsp;&nbsp;&nbsp;&nbsp;under the reports section in your YoYummy system</html>";
		lblNewLabel_1.setText(sb);
		lblNewLabel_1.setHorizontalTextPosition(JLabel.CENTER);
		lblNewLabel_1.setVerticalTextPosition(JLabel.CENTER);
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblNewLabel_1.setBounds(10, 11, 697, 90);
		panel_welcome.add(lblNewLabel_1);
		
		frame_login.setLocation(0, 0);
		frame_login.setSize(screenW, screenH);
		frame_login.setVisible(true);
	}
	
	
	public static String getWebUrl() {
		//webUrl=https\://abc.com
		Properties p = ReadINIFileModel.readWebUrl();
		String webUrl = null;
		if(p != null) 
			webUrl = p.getProperty("webUrl");
		return webUrl;
	}
	
//	protected static void orderDetailShow(ArrayList<NewsItem> order_detail_arr, String order_id) {
//		_order_id = order_id;
//		_branch_order_id =  order_detail_arr.get(0).getOrderBranch();
//		_order_counter = order_detail_arr.get(0).getOrderCounter();
//		_order_total = order_detail_arr.get(0).getOrderTotal();
//		_order_paid = order_detail_arr.get(0).getOrderPaid();
//		_order_status = order_detail_arr.get(0).getOrderStatus();
//		_order_pay_status = order_detail_arr.get(0).getOrderPayStatus();
//		_order_for = order_detail_arr.get(0).getOrderType();
//		String order_createdon = order_detail_arr.get(0).getOrderCreatedon();
//		_order_address = order_detail_arr.get(0).getOrderAddress();
//		_order_contact = order_detail_arr.get(0).getOrderContact();
//		
//		ArrayList<OrderDetailArray> orderDetail = order_detail_arr.get(0).getDetailArray();
//		int len = orderDetail.size();
//		order_name_array = new String[len];
//		order_currency_array = new String[len];
//		order_cost_array = new String[len];
//		order_qty_array = new String[len];
//		order_note_array = new String[len];
//		for(int i=0; i<orderDetail.size(); i++) {
//			String order_cost = orderDetail.get(i).getOrderCost();
//			String order_name = orderDetail.get(i).getOrderName();                      
//			String order_currency = orderDetail.get(i).getOrderCurrency();
//			String order_qty = orderDetail.get(i).getOrderQty();
//			String order_note = orderDetail.get(i).getOrderNote();
//			order_name_array[i] = order_name;
//			order_cost_array[i] = order_cost;
//			order_currency_array[i] = order_currency;
//			order_qty_array[i] = order_qty;
//			order_note_array[i] = order_note;
//		}        
//	}

	protected static JPanel createInnerPanel(String text) {
		JPanel jplPanel = new JPanel();
		JLabel jlbDisplay = new JLabel(text);
		jlbDisplay.setHorizontalAlignment(JLabel.CENTER);
		jplPanel.setLayout(new GridLayout(1, 1));
		jplPanel.add(jlbDisplay);
		return jplPanel;
	}

	protected static void loginConfirm() throws UnsupportedEncodingException, JSONException {
		//http://ec2-54-169-136-98.ap-southeast-1.compute.amazonaws.com/api/v1/customerorders?branchId=24&offset=110000&limit=10
		enter_url.setText("ec2-54-169-136-98.ap-southeast-1.compute.amazonaws.com/api/v1/customerorders");//url
		enter_app_id.setText("110000");//offset
		enter_branch_id.setText("24");//branchId
		enter_password.setText("10");//limit
		
		url = enter_url.getText().toString();
		appId = enter_app_id.getText().toString();
		branchId = enter_branch_id.getText().toString();
		password = new String(enter_password.getPassword());
		
		webAddressSave();
		
		if(!url.equals("") && !appId.equals("") && !branchId.equals("") && !password.equals("")) {
			String Url = url; 
			String AppId  = appId; 
			String BranchId = branchId;
			String Password = password;
//			String query = 	URLEncoder.encode("{" +	"\"app\"" +":"+AppId+ "," + "\"password\""+":"+"\""+Password+"\""+"}" ,"utf-8");
//			String loginUrl = "http://" + Url + "?branchId/" + BranchId + "?data=" + query;
//			
//			jsonObject = jsonParser.getJSONFromUrl(loginUrl);
//			if(jsonObject != null) {
//				checksString = jsonObject.getString("login");
//				if(checksString.equalsIgnoreCase("OK")) {	
					try {						
						LoginSwingWorker.userInfoSave(Url, AppId, BranchId, Password);
						
						mainFrameShow();	
						frame.setLocation(frame_login.getLocation().x, frame_login.getLocation().y);
						frame_login.setVisible(false);						
					} catch (Exception e) {
						e.printStackTrace();
					}
//				} else {
//					showMessage(frame_login, "Please enter valid values");
//					enter_password.selectAll();
//					enter_password.requestFocus();
//				}
//			} else {
//				showMessage(frame_login, "Please enter valid values");
//				enter_password.selectAll();
//				enter_password.requestFocus();
//			}
		} else {
			showMessage(frame_login, "Please fill all fields!");
		}
	}
	
	private static void webAddressSave() {
		_webUrl = gotoWebText.getText().trim();
		
		String w = _webUrl.substring(_webUrl.lastIndexOf("/")+1);
		
		if(!w.equals("")) {
			ReadINIFileModel.writeWebUrl(_webUrl);
		} 
	}
	public static int getRowFromId(String id, int selectedTab) {
		JTable jt;
		int row = -1;
		if(selectedTab == 0) 
			jt = table_all;
		else if(selectedTab == 1)
			jt = table_order;
		else 
			return row;
		
		for(int i = 0; i < jt.getRowCount(); i++) {
			String val = (String)jt.getValueAt(i, 1);
			if(id.equalsIgnoreCase(val)) {
				row = i;
				break;
			}
		}
		return row;
	}
	
	public static void showCircleProgress() {
		circleLabel.setVisible(true);
	}
	
	public static void hideCircleProgress() {
		circleLabel.setVisible(false);
	}
	public static void showMessage(JFrame f, String msg) {
		JOptionPane.showMessageDialog(f, msg);
	}
	public static void showMessage(JDialog setupDialog, String msg) {
		JOptionPane.showMessageDialog(setupDialog, msg);
	}
	public static void printDebugData(JTable table) {
        int numRows = table.getRowCount();
        int numCols = table.getColumnCount();
        javax.swing.table.TableModel model = table.getModel();
 
        for (int i=0; i < numRows; i++) {
            for (int j=0; j < numCols; j++) {
                System.out.print("  " + model.getValueAt(i, j));
            }
        }
    }	
		
	public static class ServerTimerTask extends TimerTask {
		@Override
		public void run(){
			if(serverTimer != null) {
				serverTimer.cancel();
				serverTimer = null;
			}
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			ArrayList<NewsItem> resultArrayNew = ListInformation.listInfo();
			if(resultArrayNew == null || resultArrayNew.size() == 0) {				
				serverTimer = new Timer();							
				serverTimerTask = new ServerTimerTask();
				serverTimer.schedule(serverTimerTask, 1000, period);
				return;
			} else {				
				int newLen = resultArrayNew.size();
				int oldLen = resultArray.size();
											
				if(newLen != oldLen) {
					resultArray = resultArrayNew;
					viewAllShow(resultArray, tableModel, 1);
				} else {			
					//====================================================================
					if(!BackgroundService.threadFlag) {
						for(int i = 0; i < newLen; i++) {
							boolean newPrintedStatus = resultArrayNew.get(i).getEMark();
							String newOrderId = resultArrayNew.get(i).getOrderId();
							
							for(int j = 0; j < oldLen; j++) {							
								boolean oldPrintedStatus = resultArray.get(j).getEMark();
								String orderId = resultArray.get(j).getOrderId();
								
								if(newOrderId != null && orderId != null && newOrderId.equals(orderId)) {
									if((newPrintedStatus && oldPrintedStatus) || (!newPrintedStatus && !oldPrintedStatus)) {
										continue;
									} else {
										resultArray = resultArrayNew;
										viewAllShow(resultArray, tableModel, 1);
										if(newPrintedStatus && !oldPrintedStatus) {
											if(PrinterTypeArray.printedOrder.contains(orderId))
												PrinterTypeArray.printedOrder.remove(orderId);
										}
									}								
								}						
							}
						}
					}
					//=======================================================================
					serverTimer = new Timer();							
					serverTimerTask = new ServerTimerTask();
					serverTimer.schedule(serverTimerTask, 1000, period);			
					return;
				}
			}
				
		}			
   }
}
