package first;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class DeleteDialog {

	public static JDialog deleteDialog;
	public static JTextField s_url;
	public static JTextField s_app_id;
	public static JTextField s_branch_id;
	public static JPasswordField s_password;
	public static JButton but_ok, but_cancel;
	
	public static String _url, _app_id, _branch_id, _password;
	private static JPanel panel;
	private static JLabel titleBox;

	/**
	 * @wbp.parser.entryPoint
	 */
	public static void createAndShowDialogGUI() {
		deleteDialog = new JDialog();
		deleteDialog.setModal(true);
		deleteDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		deleteDialog.getContentPane().setBackground(new Color(47, 79, 79));
		deleteDialog.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setBackground(new Color(46, 139, 87));
		panel.setBounds(0, 0, 494, 175);
		deleteDialog.getContentPane().add(panel);
		panel.setLayout(null);
		
		but_ok = new JButton("Ok");
		but_ok.setBounds(139, 110, 101, 33);
		but_ok.setForeground(new Color(0, 0, 0));
		but_ok.setFont(new Font("Tahoma", Font.BOLD, 16));
		but_ok.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panel.add(but_ok);
		
		but_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LoginSwingWorker.orderAndResInfoDelete(MainDesign.selectedTab);
//				PrinterTypeArray.printedOrder.clear();
//				PrinterTypeArray.printedRes.clear();
				deleteDialog.setVisible(false);
			}
		});		
		
		but_cancel = new JButton("Cancel");
		but_cancel.setBounds(265, 110, 101, 33);
		but_cancel.setForeground(new Color(0, 0, 0));
		but_cancel.setFont(new Font("Tahoma", Font.BOLD, 16));
		but_cancel.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panel.add(but_cancel);
		
		but_cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deleteDialog.setVisible(false);
			}
		});				
		
		JLabel lblNewLabel = new JLabel("Do you want to delete history ?");
		lblNewLabel.setBounds(113, 44, 273, 30);
		panel.add(lblNewLabel);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 18));
		
		titleBox = new JLabel("");
		titleBox.setFont(new Font("Tahoma", Font.BOLD, 16));
		TitledBorder titled = new TitledBorder("Delete History");
		titled.setTitleColor(Color.WHITE);
		titled.setTitleFont(new Font("Tahoma", Font.BOLD, 16));
		titleBox.setBounds(24, 11, 443, 153);
		titleBox.setHorizontalAlignment(JLabel.CENTER);
		titleBox.setBorder(titled);
		panel.add(titleBox);
		
		deleteDialog.setBackground(new Color(61, 111, 106));
		deleteDialog.setSize(500, 204);
		deleteDialog.setLocationRelativeTo(null);
		deleteDialog.setResizable(false);
		deleteDialog.setVisible(true);
	} 
}
