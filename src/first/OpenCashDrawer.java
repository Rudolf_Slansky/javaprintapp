package first;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;

public class OpenCashDrawer {

    static boolean showStatus;  // false if error
    
    public static void OpenDrawer(String dest_printer) {    	
    	
        showStatus = false;
        PrinterService ps = new PrinterService();
        PrintService pss = ps.getCheckPrintService(dest_printer);
      //==========
		try {		
			DocPrintJob doc_job = pss.createPrintJob();
			
			byte[] cmd = new byte[]{0x1B, 0x70, 0x00, 60, 120};
			
			DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
			
			Doc doc = new SimpleDoc(cmd, flavor, null);
			
			doc_job.print(doc, null);
			
			showStatus = true;
		} catch (Exception e) {					
			e.printStackTrace();
			showStatus = false;
		}
		//=========
	}
		
	public static boolean getStatus() {		
		return showStatus;		
	}
}