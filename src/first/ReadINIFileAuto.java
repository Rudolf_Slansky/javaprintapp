package first;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class ReadINIFileAuto {
	
	public static Properties pro = new Properties();
	public static OutputStream output = null;
	
	public static String readIt() {
		Properties pro = null;
		String autoCheck = null;
		try {
			pro = new Properties();
			File f = new File(ReadINIFile.currentPathString + "\\tempAuto.properties");
			if(f.isFile()) {
				pro.load(new FileInputStream(ReadINIFile.currentPathString + "\\tempAuto.properties"));
				autoCheck = pro.getProperty("AutoCheck");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return autoCheck;
	}
                 
	public static void writeIt(String autoCheck) {
			
		try {			
			output = new FileOutputStream(ReadINIFile.currentPathString + "\\tempAuto.properties");
			pro.setProperty("AutoCheck", autoCheck); 
			pro.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

