package first;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

public class ReadINIFileModel {
	
//===================== Cash Model Reading and Writing =================================
	public static Properties readIt() {
		Properties pro = null;
		try {
			pro = new Properties();
			File f = new File(ReadINIFile.currentPathString + "\\tempModel.properties");
			if(f.isFile()) {
				pro.load(new FileInputStream(ReadINIFile.currentPathString + "\\tempModel.properties"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pro;
	}
               
	public static void writeIt(String selectedCashType, String cashPrinterName, String cashNumber, String notListedCode) {	
		OutputStream output = null;
		Properties pro = null;
		try {		
			pro = new Properties();
			output = new FileOutputStream(ReadINIFile.currentPathString + "\\tempModel.properties");
			pro.setProperty("selectedCash", selectedCashType);
			pro.setProperty("cashPrinterName", cashPrinterName);
			pro.setProperty("cashNumber", cashNumber);
			pro.setProperty("notListedPrinterCode", notListedCode);
			pro.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
//=======================================================================================
//===================  Web Url Reading and Writing ======================================
	public static Properties readWebUrl() {
		Properties pro = null;
		try {
			pro = new Properties();
			File f = new File(ReadINIFile.currentPathString + "\\tempWeb.properties");
			if(f.isFile()) {
				pro.load(new FileInputStream(ReadINIFile.currentPathString + "\\tempWeb.properties"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pro;
	}
	public static void writeWebUrl(String _webUrl) {
		OutputStream output = null;
		Properties pro = null;
		try {		
			pro = new Properties();			
			output = new FileOutputStream(ReadINIFile.currentPathString + "\\tempWeb.properties");
			pro.setProperty("webUrl", _webUrl);
			pro.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
//========================================================================================
//==========================  Order Id Reading and Writing  =============================
	public static Properties readOrderId() {
		Properties pro = null;
		try {
			pro = new Properties();
			File f = new File(ReadINIFile.currentPathString + "\\tempOrder.properties");
			if(f.isFile()) {
				pro.load(new FileInputStream(ReadINIFile.currentPathString + "\\tempOrder.properties"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pro;
	}
	public static void writeOrderId(String[] orderIdArray) {
		OutputStream output = null;
		Properties pro = null;
		try {		
			pro = new Properties();			
			output = new FileOutputStream(ReadINIFile.currentPathString + "\\tempOrder.properties");
//			int k = 0;
//			for(int i = 0; i < orderIdArray.length; i++) {
//				String order_id = orderIdArray[i];
//				if(order_id.equals("0"))
//					continue;
////				pro.setProperty("orderId"+k, order_id);
//				k++;
//			}
			String preVal = "0";
			for(int i = 0; i < orderIdArray.length; i++) {
				String order_id = orderIdArray[i];
				if(order_id.equals("0"))
					continue;
				if(Integer.parseInt(order_id) > Integer.parseInt(preVal)) 
					preVal = order_id;
			}
			pro.setProperty("orderBig", preVal);
//			pro.setProperty("count", String.valueOf(k));
			
			pro.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
//==========================================================================================

}

