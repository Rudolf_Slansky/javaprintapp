package first;
//order_name_array[i] = order_name;
//order_currency_array[i] = order_currency;
//order_cost_array[i] = order_cost;
//order_qty_array[i] = order_qty;
//order_note_array[i] = order_note;
public class OrderDetailArray {
	public String order_name;
	public String order_currency;
	public String order_cost;
	public String order_qty;
	public String order_note;
	public String order_other;
	
	public void setOrderName(String mName) {
		order_name = mName;
	}
	public String getOrderName() {
		return order_name;
	}
	
	public void setOrderCurrency(String mCurrency) {
		order_currency = mCurrency;
	}
	public String getOrderCurrency() {
		return order_currency;
	}
	
	public void setOrderCost(String mCost) {
		order_cost = mCost;
	}
	public String getOrderCost() {
		return order_cost;
	}
	
	public void setOrderQty(String mQty) {
		order_qty = mQty;
	}
	public String getOrderQty() {
		return order_qty;
	}
	
	public void setOrderNote(String mNote) {
		order_note = mNote;
	}
	public String getOrderNote() {
		return order_note;
	}
	
	public void setOrderOther(String mOther) {
		order_other = mOther;
	}
	public String getOrderOther() {
		return order_other;
	}
}
