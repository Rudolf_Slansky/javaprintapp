package first;

public class MainFile {
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                MainDesign.createAndShowGUI();
            }
        });
	}
}
