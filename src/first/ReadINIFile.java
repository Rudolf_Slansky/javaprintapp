package first;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class ReadINIFile {
	
	public static Properties pro = new Properties();
	public static OutputStream output = null;
	public static Path currentPath = Paths.get(MainDesign.getRunningDir());
	public static String currentPathString = currentPath.toString();
	
	public static Properties readIt() {
		Properties pro = null;
		try {
			
			File f = new File(currentPathString + "\\temp.properties");
			if(f.isFile()) {
				pro = new Properties();			
				pro.load(new FileInputStream(currentPathString + "\\temp.properties"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pro;
	}
                 
	public static void writeIt(String[] order_printers, String o_chk) {
			
		try {			
			output = new FileOutputStream(currentPathString + "\\temp.properties");
			
			if(order_printers != null) {
				int len1 = order_printers.length;
				for(int i=0; i<len1; i++) {
					pro.setProperty("orderPrinter"+i, order_printers[i]);	
				}				
				pro.setProperty("orderCheck", o_chk);
				pro.setProperty("orderNum", String.valueOf(len1));
			} 
			pro.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
