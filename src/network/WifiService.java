package network;

import ipAdapter.Tokenizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Logger;

import javax.naming.Context;


public class WifiService extends PrintService implements PrinterClass {
	
	public static String _ip, _gateway;
	public static ArrayList<Device> deviceList=new ArrayList<Device>();
	
	private static final String TAG = "WifiService";
	int port=10000;
	TcpClientClass tcpClient=null;
	DatagramPacket packet;
	private static DatagramSocket socket;
	private DatagramPacket getpacket;
	private byte data2[] = new byte[4 * 1024];
//	WifiManager mWm = null;
//	ProgressDialog dialog;
	boolean sendDatagram = true;
	static Th th = null;
	int scanState=0;

	Context context;
	Handler mhandler, handler;

	public WifiService(Context _context, Handler _mhandler, Handler _handler) {
		context = _context;
		mhandler = _mhandler;
		handler = _handler;
		
		tcpClient=new TcpClientClass(mhandler);
	}

//	@Override
//	public boolean open(Context context) {
//		return setWifi(context, true);
//	}
//
//	@Override
//	public boolean close(Context context) {
//		setWifi(context, false);
//		return false;
//	}

	public  void scan() {
		try {
			th = new Th();
			th.start();
			
			String strIp=getIp();
			strIp=strIp.substring(0, strIp.lastIndexOf('.'));
			strIp+=".255";
			socket = new DatagramSocket();
			InetAddress serverAddress = InetAddress.getByName(strIp);
			String str = "AT+FIND=?\r\n";
			byte data[] = str.getBytes();
			packet = new DatagramPacket(data, data.length, serverAddress, 10002);

//			String getwayIP=tcpClient.getGatewayIPAddress(context);
			String getwayIP = getOwnGetwayIp();
			connect(getwayIP);
			if(tcpClient.isConnected())
			{
				sendDatagram=false;
				//tcpClient.DisConnect();
				Device d = new Device();
				d.deviceName = getwayIP;
				d.deviceAddress = getwayIP;
//				Message msg = new Message();
//				msg.what = 1;
//				msg.obj = d;
//				handler.sendMessage(msg);
			}
			else
			{
				new MyThread().start();
	
//				Message msg = new Message();
//				msg.obj = "���������豸";
//				mhandler.sendMessage(msg);
	
				setState(PrinterClass.STATE_SCANING);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stopScan() {
		th=null;
		sendDatagram=false;
		setState(PrinterClass.STATE_SCAN_STOP);
	}

	@Override
	public boolean connect(String device) {
		tcpClient.Connect(device, port);
		setState(PrinterClass.STATE_CONNECTED);
		return true;
	}

	@Override
	public boolean disconnect() {
		tcpClient.DisConnect();
		return true;
	}

	@Override
	public int getState() {
		return scanState;
	}

	@Override
	public boolean IsOpen() {
		return false;
//		if (mWm != null) {
//			return mWm.isWifiEnabled();
//		} else {
//			return false;
//		}
	}

	@Override
	public boolean write(byte[] bt) {
		if(getState()!= PrinterClass.STATE_CONNECTED) 
			return false;
		
		tcpClient.SendData(bt);
		return true;
	}

	@Override
	public boolean printText(String textStr) {
		byte[] buffer = getText(textStr);
		
		if (buffer.length <= 100) {
			write(buffer);
		}
		int sendSize = 100;
		int issendfull=0;
		for (int j = 0; j < buffer.length; j += sendSize) {

			byte[] btPackage = new byte[sendSize];
			if (buffer.length - j < sendSize) {
				btPackage = new byte[buffer.length - j];
			}
			System.arraycopy(buffer, j, btPackage, 0, btPackage.length);
			write(btPackage);

			try {
				Thread.sleep(86);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (PrintService.isFUll) {
				Logger.getGlobal().info("BUFFER FULL");
				int index = 0;
				while (index++ < 500) {
					if (!PrintService.isFUll) {
						issendfull=0;
						Logger.getGlobal().info("BUFFER NULL"+index);
						break;
					}
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return true;
		////return write(getText(textStr));
	}

	@Override
	public boolean printUnicode(String textStr) {
		return write(getTextUnicode(textStr));
	}
//	public boolean setWifi(Context context, boolean isEnable) {
//		return false;
////		if (mWm == null) {
////			mWm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
////			// return;
////		}
////		return	mWm.setWifiEnabled(isEnable);
//	}

	public class MyThread extends Thread {
		public void run() {
			int timeSpan=0;
			while (sendDatagram) {
				try {
					if(timeSpan++>20)
					{
						setState(PrinterClass.STATE_SCAN_STOP);
						timeSpan=0;
						break;
					}
					socket.send(packet);
					Thread.sleep(500);
				} catch (IOException e) {
					e.printStackTrace();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	class Th extends Thread {

		@Override
		public void run() {
			boolean b = true;
			try {
				getpacket = new DatagramPacket(data2, data2.length);
			} catch (Exception e) {
			}
			while (b) {
				try {
					socket.receive(getpacket);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if (getpacket.getAddress() != null) {
					String ipStr = getpacket.getAddress().toString().substring(1);
					String macAdd = new String(data2, 0, getpacket.getLength()).trim();

					Device d = new Device();
					d.deviceName = macAdd;
					d.deviceAddress = ipStr;
//					Message msg = new Message();
//					msg.what = 1;
//					msg.obj = d;
//					handler.sendMessage(msg);
				}
			}
		}
	}

	@Override
	public void setState(int state) {
		Logger.getGlobal().info("setState:"+state);
		scanState= state;
	}

	@Override
	public ArrayList<Device> getDeviceList() {
		return null;
	}
	public static String getIp() {
		try {
            Process pro = Runtime.getRuntime().exec("cmd.exe /c route print");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(pro.getInputStream())); 

            String line;
            while((line = bufferedReader.readLine())!=null)
            {
                line = line.trim();
                String [] tokens = Tokenizer.parse(line, ' ', true , true);// line.split(" ");
                if(tokens.length == 5 && tokens[0].equals("0.0.0.0")) {
                    _gateway = tokens[2];
                    _ip = tokens[3];
                    return _ip;
                }
            }
//            pro.waitFor();
			     
        }
        catch(IOException e) {
            System.err.println(e);
            e.printStackTrace();
        }
		return _ip;
	}
	public static String getOwnGetwayIp() {
		try {
            Process pro = Runtime.getRuntime().exec("cmd.exe /c route print");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(pro.getInputStream())); 

            String line;
            while((line = bufferedReader.readLine())!=null)
            {
                line = line.trim();
                String [] tokens = Tokenizer.parse(line, ' ', true , true);// line.split(" ");
                if(tokens.length == 5 && tokens[0].equals("0.0.0.0")) {
                    _gateway = tokens[2];
                    _ip = tokens[3];
                    return _gateway;
                }
            }
//            pro.waitFor();
			     
        }
        catch(IOException e) {
            System.err.println(e);
            e.printStackTrace();
        }
		return _gateway;
	}
	 
	 private String intToIp(int i) {
	     return (i & 0xFF ) + "." +
	     ((i >> 8 ) & 0xFF) + "." +
	     ((i >> 16 ) & 0xFF) + "." +
	     ( i >> 24 & 0xFF) ;
	 }
	 public ArrayList getListData() {
	 if(deviceList!=null) 
		deviceList.clear();
		scan();				
		deviceList = getDeviceList();
		return deviceList;
	 }
	@Override
	public boolean open(Context context) {
		return false;
	}

	@Override
	public boolean close(Context context) {
		return false;
	} 
}
