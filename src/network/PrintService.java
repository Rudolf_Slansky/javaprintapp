package network;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class PrintService{
	public static boolean isFUll=false;

	public static int imageWidth=24;
	
	public byte[] getText(String textStr) {
		// TODO Auto-generated method stubbyte[] send;
		byte[] send=null;
		try {
			send = textStr.getBytes("GBK");
		} catch (UnsupportedEncodingException e) {
			send = textStr.getBytes();
		}
		return send;
	}

	/**
	 * change the text to unicode byte array
	 * @param textStr String text
	 * @return unicode byte array
	 */
	public byte[] getTextUnicode(String textStr) {
		// TODO Auto-generated method stub
		byte[] send=string2Unicode(textStr);
		return send;
	}
	
	
	
	private static byte[] string2Unicode(String s) {   
	    try {   
	      StringBuffer out = new StringBuffer("");   
	      byte[] bytes = s.getBytes("unicode");   
	    	byte[] bt=new byte[bytes.length-2];
	      for (int i = 2,j=0; i < bytes.length - 1; i += 2,j += 2) {   
	    	  bt[j]= (byte)(bytes[i + 1] & 0xff);   
	    	  bt[j+1] = (byte)(bytes[i] & 0xff);    
	      }   
	      return bt;   
	    }   
	    catch (Exception e) {   
	      try {
			byte[] bt=s.getBytes("GBK");
			return bt;
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return null;
		}
	    }   
	  }
	
	
	

	public void encodeYUV420SP(byte[] yuv420sp, int[] rgba, int width,
			int height) {
		final int frameSize = width * height;
		int[] U, V;
		U = new int[frameSize];
		V = new int[frameSize];
		final int uvwidth = width / 2;
		int r, g, b, y, u, v;
		int bits = 8;
		int index = 0;
		int f = 0;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				r = (rgba[index] & 0xff000000) >> 24;
				g = (rgba[index] & 0xff0000) >> 16;
				b = (rgba[index] & 0xff00) >> 8;
				// rgb to yuv
				y = ((66 * r + 129 * g + 25 * b + 128) >> 8) + 16;
				u = ((-38 * r - 74 * g + 112 * b + 128) >> 8) + 128;
				v = ((112 * r - 94 * g - 18 * b + 128) >> 8) + 128;
				// clip y
				// yuv420sp[index++] = (byte) ((y < 0) ? 0 : ((y > 255) ? 255 :
				// y));
				byte temp = (byte) ((y < 0) ? 0 : ((y > 255) ? 255 : y));
				yuv420sp[index++] = temp > 0 ? (byte) 1 : (byte) 0;

				// {
				// if (f == 0) {
				// yuv420sp[index++] = 0;
				// f = 1;
				// } else {
				// yuv420sp[index++] = 1;
				// f = 0;
				// }

				// }

			}

		}
		f = 0;
	}
	
	/**
	 * ׷���ļ���ʹ��FileOutputStream���ڹ���FileOutputStreamʱ���ѵڶ���������Ϊtrue
	 * 
	 * @param fileName
	 * @param content
	 */
	public void createFile(String path, byte[] content) throws IOException {
		FileOutputStream fos = new FileOutputStream(path);
		fos.write(content);
		fos.close();
	}
}
