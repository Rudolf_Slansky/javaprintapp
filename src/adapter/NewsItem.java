package adapter;

import java.util.ArrayList;

import first.OrderDetailArray;


public class NewsItem {
	public String order_id, trans_id, order_date, order_conf_status, print_status, order_type, payment_type, order_time, sub_total, customer_id;
	public boolean emarkFlag;
	
	//======= for email ============
	public String branch_name, branch_tel, branch_email, app;
	
	ArrayList<OrderDetailArray> detailArray; 
	//=== ArrayList Start ==========
	public ArrayList<OrderDetailArray> getDetailArray() {
		return detailArray;
	}
	public void setDetailArray(ArrayList<OrderDetailArray> mDetailArray) {
		this.detailArray = mDetailArray;
	}
	
	public String getOrderId() {
		return order_id;
	}
	public void setOrderId(String order_id) {
		this.order_id = order_id;
	}	
	public String getTransId() {
		return trans_id;
	}
	public void setTransId(String trans_id) {
		this.trans_id = trans_id;
	}
	
	public String getOrderDate() {
		return order_date;
	}
	public void setOrderDate(String order_date) {
		this.order_date = order_date;
	}
	
	public String getOrderConfirmationStatus() {
		return order_conf_status;
	}
	public void setOrderConfirmationStatus(String order_conf_status) {
		this.order_conf_status = order_conf_status;
	}	
	
	public String getPrintingStatus() {
		return print_status;
	}
	public void setPrintingStatus(String print_status) {
		this.print_status = print_status;
	}
	
	public String getOrderType() {
		return order_type;
	}
	public void setOrderType(String order_type) {
		this.order_type = order_type;
	}
	
	public String getPaymentType() {
		return payment_type;
	}
	public void setPaymentType(String payment_type) {
		this.payment_type = payment_type;
	}	
	
	public String getOrderTime() {
		return order_time;
	}
	public void setOrderTime(String order_time) {
		this.order_time = order_time;
	}
		
	public String getSubTotal() {
		return sub_total;
	}
	public void setSubTotal(String sub_total) {
		this.sub_total = sub_total;
	}
	
	public String getCustomerId() {
		return customer_id;
	}
	public void setCustomerId(String customer_id) {
		this.customer_id = customer_id;
	}
	
	public boolean getEMark() {
		return emarkFlag;
	}
	public void setEMark(boolean emarkFlag) {
		this.emarkFlag = emarkFlag;
	}
	
	@Override
	public String toString() {
		return "[order_id=" + order_id + "]";
	}
}

