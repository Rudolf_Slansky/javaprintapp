package adapter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.media.Log;

import first.JSONParser;
import first.LoginSwingWorker;
import first.MainDesign;
import first.OrderDetailArray;

public class ListInformation {
	
	public static String Url, AppId, BranchId, Password;
	
	public static JSONParser jsonParser = new JSONParser();;
	public static JSONObject jsonObject, jOrderDetailObject;
	public static JSONArray jOrderArray = new JSONArray(), itemArray;
	
	public static ArrayList<NewsItem> results;
	public static ArrayList<NewsItem> orderData;
	public static ArrayList<OrderDetailArray> detailArray;
	
	public static int order_arr_len, len;
	public static String[] orderIdArray;
	public static String[] order_name_array, order_currency_array, order_cost_array, order_qty_array, order_note_array, order_other_array;
	
	public static String order_id;
//	Transaction ID | Order Date | Order Status             | Print Status   | Order Type | Payment Type | Order Time	| Subtotal	 | Customer ID 
//	----------------------------------------------------------------------------------------------------------------------------------------------
//	transId        | orderDate  | orderConfirmationStatus  | printingStatus | orderType  | paymentType  | orderTime     | subTotal   | customerId
	public static String trans_id, order_date, order_conf_status, print_status, order_type, payment_type, order_time, sub_total, customer_id;
		
	
	public static ArrayList<NewsItem> listInfo() {
		ArrayList<NewsItem> totalData = new ArrayList<NewsItem>();
		
			try {
				String info = LoginSwingWorker.userInfoRead();
				if(!info.equals("")) {
					int a = info.lastIndexOf("/");
					int b = info.indexOf("|");
					int c = info.indexOf("=");
					Url = info.substring(0, a);
					AppId = info.substring(a+1, b);
					BranchId = info.substring(b+1, c);
					Password = info.substring(c+1);
				}
				//http://ec2-54-169-136-98.ap-southeast-1.compute.amazonaws.com/api/v1/customerorders?branchId=24&offset=110000&limit=10
				String orderUrl = "http://" + Url + "?branchId=" + BranchId + "&offset=" + AppId + "&limit=" + Password;
				jOrderArray = jsonParser.getJSONArrayFromUrl(orderUrl);
				System.out.print(jOrderArray);
				if(jOrderArray != null) {
					totalData = orderListShow(jOrderArray);					
				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
		return totalData;
	}

	public static ArrayList<NewsItem> orderInfo() {
		ArrayList<NewsItem> totalData = null;
		
		try {
			String info = LoginSwingWorker.userInfoRead();
			if(!info.equals("")) {
				int a = info.lastIndexOf("/");
				int b = info.indexOf("|");
				int c = info.indexOf("=");
				Url = info.substring(0, a);
				AppId = info.substring(a+1, b);
				BranchId = info.substring(b+1, c);
				Password = info.substring(c+1);
			}
			String orderUrl = "http://" + Url + "?branchId=" + BranchId + "&offset=" + AppId + "&limit=" + Password;		
			jOrderArray = jsonParser.getJSONArrayFromUrl(orderUrl);
			
			if(jOrderArray != null) {
				totalData = orderListShow(jOrderArray);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return totalData;
	}
	
	
//	Transaction ID | Order Date | Order Status             | Print Status   | Order Type | Payment Type | Order Time	| Subtotal	 | Customer ID 
//	----------------------------------------------------------------------------------------------------------------------------------------------
//	transId        | orderDate  | orderConfirmationStatus  | printingStatus | orderType  | paymentType  | orderTime     | subTotal   | customerId
	private static ArrayList<NewsItem> orderListShow(JSONArray jOrderArray) {
		
		System.out.print(jOrderArray);
		
		try {				
			results = new ArrayList<NewsItem>();
			ArrayList<NewsItem> result1 = new ArrayList<NewsItem>();
			ArrayList<NewsItem> result2 = new ArrayList<NewsItem>();
			if(jOrderArray != null){

				order_arr_len = jOrderArray.length();
				orderIdArray = new String[order_arr_len];
				
				result1 = new ArrayList<NewsItem> ();
				result2 = new ArrayList<NewsItem> ();
				
				for(int i=0; i<order_arr_len; i++){
					NewsItem newsData = new NewsItem();
					JSONObject childObj = jOrderArray.getJSONObject(i);
					int order_id_d = childObj.getInt("id");
					order_id = String.valueOf(order_id_d);
					trans_id = childObj.getString("transId");
					order_date = childObj.getString("orderDate");
					order_conf_status = childObj.getString("orderConfirmationStatus");
					print_status = childObj.getString("printingStatus");
					order_type = childObj.getString("orderType");
					payment_type = childObj.getString("paymentType");
					order_time = childObj.getString("orderTime");
					double sub_total_d = childObj.getDouble("subTotal");
					sub_total = String.valueOf(sub_total_d);
					int customer_id_d = childObj.getInt("customerId");
					customer_id = String.valueOf(customer_id_d);
					
					newsData.setOrderId(order_id);
					newsData.setTransId(trans_id);
					newsData.setOrderDate(order_date);
					newsData.setOrderConfirmationStatus(order_conf_status);
					newsData.setPrintingStatus(print_status);
					newsData.setOrderType(order_type);
					newsData.setPaymentType(payment_type);
					newsData.setOrderTime(order_time);
					newsData.setSubTotal(sub_total);
					newsData.setCustomerId(customer_id);
					
					if(print_status.equals("NEVER_PRINTED")) {
						newsData.setEMark(true);						
					} else {
						newsData.setEMark(false);
					}
					
					if(print_status.equals("NEVER_PRINTED")) {
						result1.add(newsData);
					} else {
						result2.add(newsData);
					}		
				}
				//============= sort for system id from low to high,  sort for red mark ========================
				if(result1 != null) {
					Collections.sort(result1, new Comparator<NewsItem>() {
						@Override
						public int compare(NewsItem e1, NewsItem e2) {
							return e2.getOrderId().compareToIgnoreCase(e1.getOrderId());
						}
					});
					results.addAll(result1);
				}
				if(result2 != null) {
					Collections.sort(result2, new Comparator<NewsItem>() {
						@Override
						public int compare(NewsItem e1, NewsItem e2) {
							return e2.getOrderId().compareToIgnoreCase(e1.getOrderId());
						}
					});
					results.addAll(result2);
				}
				//===============================================================================================
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return results;
	}
	
	public static ArrayList<NewsItem> detailOrderData(String order_id) {
		orderData = new ArrayList<NewsItem>();
		try {
			String info = LoginSwingWorker.userInfoRead();
			if(!info.equals("")) {
				int a = info.lastIndexOf("/");
				int b = info.indexOf("|");
				int c = info.indexOf("=");
				Url = info.substring(0, a);//url
				AppId = info.substring(a+1, b);//offset
				BranchId = info.substring(b+1, c);//branchId
				Password = info.substring(c+1);//limit
			}
			String orderUrl = "http://" + Url + "?branchId=" + BranchId + "&offset=" + AppId + "&limit=" + Password;		
			jOrderArray = jsonParser.getJSONArrayFromUrl(orderUrl);
			if(jOrderArray != null) {
				orderData = orderListShow(jOrderArray);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return orderData;
	}

//	public static ArrayList<NewsItem> orderListShow(JSONObject jOrderDetailObject2) {
//		try {			
//			trans_id = jOrderDetailObject2.getString("");
//			order_date = jOrderDetailObject2.getString(""); 
//			order_conf_status = jOrderDetailObject2.getString("");
//			print_status = jOrderDetailObject2.getString("");
//			order_type = jOrderDetailObject2.getString("");
//			payment_type = jOrderDetailObject2.getString("");
//			order_time = jOrderDetailObject2.getString("");
//			sub_total = jOrderDetailObject2.getString("");
//			customer_id = jOrderDetailObject2.getString("");
//			
//			String _order_items = jOrderDetailObject2.getString("items");
//			itemArray = new JSONArray(_order_items);
//			
//			len = itemArray.length();
//			order_name_array = new String[len];
//			order_currency_array = new String[len];
//			order_cost_array = new String[len];
//			order_qty_array = new String[len];
//			order_note_array = new String[len];
//			order_other_array = new String[len];
//			
//			detailArray = new ArrayList<OrderDetailArray>();
//			
//			for(int i=0; i < len; i++){
//				OrderDetailArray o_detailArray = new OrderDetailArray();
//				JSONObject itemObject = itemArray.getJSONObject(i);
//				String order_name = itemObject.getString("order_name");
//				String order_currency = itemObject.getString("currency");
//				String order_cost = itemObject.getString("cost");
//				String order_qty = itemObject.getString("qty");
//				String order_note = itemObject.getString("special_notes");
//				String order_other = itemObject.getString("other_charges");
//				
//				o_detailArray.setOrderCost(order_cost);
//				o_detailArray.setOrderName(order_name);
//				o_detailArray.setOrderCurrency(order_currency);
//				o_detailArray.setOrderQty(order_qty);
//				o_detailArray.setOrderNote(order_note);
//				o_detailArray.setOrderOther(order_other);
//				
//				detailArray.add(i, o_detailArray);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		
//		ArrayList<NewsItem> detailOrderData = new ArrayList<NewsItem>();
//		NewsItem preOrderData = new NewsItem();		
//		preOrderData.setOrderBranch(_branch_order_id);
//		preOrderData.setOrderCounter(_order_counter);
//		preOrderData.setOrderStatus(_order_status);
//		preOrderData.setOrderTotal(_order_total);
//		
//		preOrderData.setDetailArray(detailArray);
//		
//		detailOrderData.add(preOrderData);
//		return detailOrderData;
//	}
	
}
